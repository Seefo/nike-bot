################################
# Base layer - setup for tooling
FROM python:3.11 as base

ARG USER_NAME=nikebot
ARG UID=1000
ARG GID=1000
RUN groupadd -g $GID $USER_NAME; useradd -lrm -u $UID -g $GID $USER_NAME
ENV USER $USER_NAME

RUN pip install --no-cache-dir --upgrade pip

WORKDIR /workspace
COPY . /workspace

################################
# Install dev & test deps
FROM base as dev

ARG BUILD=false

RUN if [ "$BUILD" = true ]; then \
        pip install --no-cache-dir -e .[test]; \
    fi

################################
# Install production requirements only
FROM base as build

RUN python setup.py bdist_wheel

FROM python:3.11-slim as prod

WORKDIR /workspace
COPY --from=build /workspace/requirements.txt /workspace
RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt

COPY --from=build /workspace/dist/*.whl /workspace/dist/
RUN pip install /workspace/dist/*.whl

CMD ["python", "-m", "nike_bot"]
