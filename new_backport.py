import datetime as dt
import json
import logging
import os
import time

from pprint import pformat

import discord
import uvloop

from discord.ext import commands

from nike_bot.chal import get_tournaments, init_challonge
from nike_bot.config import env
from nike_bot.guild_config import GuildConfig
from nike_bot.redis import WinTimerRedis
from nike_bot.tournament_manager import TournamentManager, TournamentState
from nike_bot.tournament_stats import TournamentStats
from nike_bot.user_stats import UserStats
from nike_bot.utils import args_get_members


uvloop.install()

bot = commands.Bot(command_prefix="!")
win_timer = WinTimerRedis()

logger = logging.getLogger(__name__)

TOURNEYS_FILE = "tourneys.json"

name_map = {
    "rebel": ["misterrebel"],
    "bega": "begawkology",
    "xeph": "xephium",
    "bran": "b.",
    "prod": "theprodigy1",
    "theprod": "theprodigy1",
    "prodigy": "theprodigy1",
    "legend": "alegendv1",
    "insta": "instamole",
    "charles": "",
    # "Too Hot" : [],
    "too hot": "A!",
    "icy": "A!",
    "too icy": "A!",
    "a.": "A!",
    "optimus": "op optimus",
    "op": "op optimus",
    "xak": "xakuvo",
    "razor": "razorclad",
    "weese": "weesesnaw",
    "lash": "lashxx",
    "chaotic": "ch4otic bane",
    "tyler": "tyler alvarez-badfish",
    "sergio": "",
    "rocker": "rocker4life98",
    "rocker's girl": "",
    "ant": "antlowery",
    "gharbage": "gahbage",
    "machu rocker too hot": ["machu", "rocker4life98", "A!"],
    "swabbie lash nickaf": ["swabbie", "lashxx", "nickaf"],
    "bega insta cobra": ["begawkology", "instamole"],
    "thicc-n-tasty chaotic owl": ["T h i c c - N - T a s t y", "ch4otic bane", "owl"],
    "rebel teddy xephium": ["misterrebel", "teddy", "xephium"],
    "botch jbomb sexymanlyman": ["botch", "jbomb"],
    "2dwaffle jmac synthetic": ["2D Waffle™", "jmac HD", "synthetic"],
    "psycho": "",
    "warp": "",
    "waffle": "2D Waffle™",
    "chrome": "",
    "bigice": "bigice",
    "bigice457": "bigice",
    "jmac": "Jmac hd",
    "cookie": "",
    "ketts": "SCRT ketts",
    "squizzle": "dasquizzle",
    "stealth star": "",
    "ihavebananaforu": "sammyboy23",
    "zero2": "dm4sho",
    "zero": "dm4sho",
    "tdy": "ᵗᵈʸ",
    "zaxday": "ᵗᵈʸ",
    "bigicy": "bigice",
    "synethic": "synthetic",
    "trptman": "",
    "k": "kntihasdbfh",
    "kingmann": "kingmaan",
    "hittin": "hittin1",
    "b": "b.",
    "offandon": "offandongamer",
    "rage": "ragefam",
    "2d waffle": "2D Waffle™",
    "stealth": "",
    "rashford": "",
    "deity": "deitylink",
    "lexie": "lexieloo",
    "cupofhottea": "",
    "elite": "eliteeliminator",
    "ooferefo": "OoferefoO",
    "hiro": "hirostream",
    "star": "",
    "oppie": "",
    "dlchamp": "",
    "u4me": "",
    "u4me2015": "",
    "oof": "OoferefoO",
    "nachos": "American Nacho",
    "hound": "houndofspace",
    "stonertortiose": "stonertortoise",
    "kingman": "kingmaan",
    "stoner": "stonertortoise",
    "squiz": "dasquizzle",
    "oreo": "It'sBrittanyBish",
    "razen": "razendorph",
    "skyzer0": "chris",
    "shank": "shankster0095",
    "skater": "",
    "nacho": "American Nacho",
    "guitair": "",
    "opulence": "",
    "zylia": "",
    "nick304": "",
    "sam.": "sammyboy23",
    "yas_drags": "",
    "salomaya": "",
    "salmoya": "",
    "squizz": "DaSquizzle",
    "dasquizz": "DaSquizzle",
    "eric": "ericxcbs",
    "jebus": "",
    "dongerlord": "thedongerlord",
    "sub": "",
    "gt": "",
    "too---icy": "A!",
    "papadoc": "",
    "nice shot!": "",
    "on4air": "",
    "bacon": "",
    "𝓑𝓾𝓮𝓷𝓬𝔂": "",
    "caribou": "",
}


def twos_tourney(teams):
    total_len = 0
    for i, v in enumerate(list(teams.values()), 1):  # noqa
        total_len += len(v)
    if (total_len / i) > 2:
        return False
    return True


def sanitize_names(names, guild):
    parsed_names = []
    logger.info("all names: %s", names)
    for name in names:
        name = name.strip().lower()
        if name in name_map:
            name = name_map[name]
        if not name:
            continue
        if not isinstance(name, list):
            name = [name]
        for n in name:
            n = n.lower()
            logger.info("name: %s", n)
            member = discord.utils.find(lambda x: x.name.lower() == n, guild.members)
            if not member:
                raise ValueError("Couldn't find user: %s", n)
            parsed_names.append(member.name)
    # convert to set temporarily to get rid of duplicates, then  return as list
    return list(set(parsed_names))


async def correct_teams(tourney, guild):
    # teams =
    # Squirrel away teams
    # remove all teams & players
    # recreate teams with newly sanitized names
    original_teams = tourney.teams.copy()
    for team, members in original_teams.items():
        await tourney.remove_team(team)
        players = sanitize_names(members, guild)
        logger.info("Team: %s", team)
        logger.info("Players: %s", players)
        await tourney.add_team(team, players)


async def finalize_streaks(winners, guild):
    guild_config = await GuildConfig.from_redis(guild.id)
    for key in winners:
        winner = await UserStats.from_redis(key)
        win_dates = sorted(list(winner.win_dates))
        logger.warning("User Stats: %s", pformat(winner.as_dict()))

        # Start at the earliest sorted win
        winner.start_win_time = (
            dt.datetime.fromtimestamp(win_dates[0])
            .replace(hour=23, minute=59, second=59, microsecond=0)
            .timestamp()
        )
        logger.warning("User Stats: %s", pformat(winner.as_dict()))
        # Append now, as a "finalizer". if they are overdue, this will kick in the logic
        # to end the winstreak.
        win_dates.append(time.time())

        for ind, wdate in enumerate(win_dates, 1):
            # enumerate starts @ 0, but we want to start at 1 since our first element
            # is what we are going to use as baseline to compare with the next date
            # we also still do a +1 on the end comparison at the bottom since
            # the final date is "now" ie its not a real win date anyway.
            if not winner.current_winner:
                logger.info("No longer champion, adding them as champion again")
                winner.current_winner = True
                winner.start_win_time = (
                    dt.datetime.fromtimestamp(wdate)
                    .replace(hour=23, minute=59, second=59, microsecond=0)
                    .timestamp()
                )
            possible_end_time = (
                dt.datetime.fromtimestamp(wdate)
                + dt.timedelta(1)
                - dt.timedelta(seconds=1)
            ).timestamp()
            possible_end_time += guild_config.winner_timeout
            logger.info("THIS win date: %s", wdate)
            logger.info("Possible End time: %s", possible_end_time)
            logger.info("NEXT win date: %s", win_dates[ind])
            if possible_end_time < win_dates[ind]:
                logger.info("Win time: %s", winner.start_win_time)
                logger.info("Result: %s", possible_end_time - winner.start_win_time)
                await winner.end_win_time(possible_end_time)
            logger.info("break check: %s vs. %s", ind + 1, len(winner.win_dates))
            if ind == len(winner.win_dates):
                break
        if winner.current_winner:
            # In case they are still on streak, lets store the hacked i
            # current_winner/start_win_time this ensures the state is appropriate,
            # so when they timeout in the future its good
            logger.info("STILL ACTIVE WINNER!: %s", guild.get_member(key[1]))
            await winner.store()

            await win_timer.add_winner_timeout(
                *key, possible_end_time
            )  # guild id, member i


async def load_tournaments():
    # get tourneys
    if os.path.isfile(TOURNEYS_FILE):
        logger.info("Loading from file")
        with open(TOURNEYS_FILE) as f:
            tournaments = json.load(f)
    else:
        logger.info("Getting from API and storing to file")
        tournaments = sorted(
            await get_tournaments(), key=lambda x: x["started_at"].timestamp()
        )
        with open(TOURNEYS_FILE, "w") as f:
            parsed = []
            for t in tournaments:
                t = (t["name"], t["url"], t["id"])
                parsed.append(t)
            json.dump(parsed, f)
        tournaments = parsed
        print("Tournaments: %s", pformat(tournaments))
    return tournaments


async def replay_from_challonge(guild, chal_user=None, chal_token=None):
    init_challonge(chal_user, chal_token)
    tournaments = await load_tournaments()
    # Keep a list of all winners, we need to do a second pass
    # and get all their end dates sorted so they have proper streaks
    winners = set()
    winner_names = set()
    player_names = set()

    for data in tournaments:
        logger.info("Data: %s", data)
        name, url, id_ = data
        tourney = await TournamentManager.from_challonge(id_, guild.id)
        # We correct them here so we can detect if its a 2s or 3s tournament
        await correct_teams(tourney, guild)
        if twos_tourney(tourney.teams):
            await tourney.cancel()
            continue
        logger.info("Tourney info: %s", pformat(tourney.info()))
        # fake the tournament start
        tourney._state = TournamentState.STARTED
        # Finalize it so we can the results in a stable format.
        results = await tourney.finish()
        # Seems weird...but this gives us a consistent timestamp for when the
        # tournament counts. our start times are "consistently" 6:30 eastern so
        # this ensure we have stable end date too
        tdate = tourney.start_time.replace(
            hour=23, minute=59, second=59, microsecond=0
        ).timestamp()
        # We have to recorrect the teams here because challonge has bad names.
        for place, team in results.items():
            players = tourney.teams[team["team"].lower()]
            members = args_get_members(players, guild.members)
            logger.info("Members: %s", members)
            for mem in members:
                # if mem.name.lower() != "bucklemyshoe":
                #    continue

                key = (guild.id, mem.id)
                win_stats = await UserStats.from_redis(key)

                # logger.warning("User Stats: %s", pformat(win_stats.as_dict()))
                if place == 1:
                    winner_names.add(mem.name)
                    await win_stats.update_win(tdate)
                    winners.add(key)
                else:
                    player_names.add(mem.name)
                    await win_stats.update_not_win(tdate, place=place)

    await finalize_streaks(winners, guild)
    gstats = await TournamentStats.from_redis(guild.id)
    stats = await gstats.stats()
    logger.info("winners: \n%s", winner_names)
    logger.info("Tournament stats: %s", stats)
    logger.info("Unique Players: %s", len(stats["Unique Players"]))


@bot.event
async def on_ready():
    logger.info("We have logged in as %s", bot.user)
    guild = discord.utils.get(bot.guilds, name="Seefo")

    await win_timer.connect()
    await replay_from_challonge(guild)


def main(token):

    logging.basicConfig(level=env.str("LOG_LEVEL"),)
    logging.getLogger("discord").setLevel("INFO")
    logging.getLogger("discord.gateway").setLevel("WARNING")
    logging.getLogger("websockets").setLevel("WARNING")
    logging.getLogger("twitchio.websocket").setLevel("WARNING")

    bot.run(token)


if __name__ == "__main__":
    try:
        token = env.str("DISCORD_BOT_TOKEN")
    except KeyError as e:
        raise KeyError(
            "Please put token in DISCORD_BOT_TOKEN environment variable"
        ) from e
    main(token)
