import datetime as dt

import pytest

from nike_bot.user_stats import UserStats


@pytest.fixture
async def ustats(us_redis):
    return await UserStats.from_redis((1, 1), us_redis)


@pytest.mark.asyncio
async def test_user_stats(ustats):
    """
    large test case, tests math, and storage/loading
    """

    result = ustats.as_dict()
    validate = dict(
        wins=0,
        start_win_time=0,
        current_winner=False,
        win_dates=list(),
        total_win_time=0,
        longest_time_streak=0,
    )
    assert all(item in result.items() for item in validate.items())

    await ustats.update_win(500000)
    await ustats.update_win(550000)
    await ustats.update_win(550000)
    result = ustats.as_dict()
    validate = dict(
        wins=2, start_win_time=500000, current_winner=True, win_dates=[432000, 518400],
    )
    assert all(item in result.items() for item in validate.items())
    await ustats.end_win_time(600000)
    assert ustats.as_dict()["total_win_time"] == 100000
    assert ustats.as_dict()["longest_time_streak"] == 100000

    # Test storage/loading
    ustats.wins = 42
    new_stats = await UserStats.from_redis((ustats.gid, ustats.uid), ustats.us_redis)
    result = new_stats.as_dict()
    validate = dict(
        wins=2,
        start_win_time=0,
        current_winner=False,
        win_dates=[432000, 518400],
        total_win_time=100000,
        longest_time_streak=100000,
    )
    assert all(item in result.items() for item in validate.items())


@pytest.mark.asyncio
async def test_win_time_stale(ustats):

    one_day = 24 * 60 * 60

    date = dt.datetime(2019, 11, 1).timestamp()
    await ustats.update_win(date)
    await ustats.end_win_time(date + one_day)
    assert ustats.longest_time_streak == one_day

    date = dt.datetime(2019, 11, 3, 12, 12, 12).timestamp()
    await ustats.update_win(date)
    await ustats.end_win_time(date + one_day)

    assert ustats.total_win_time == 2 * one_day
    assert ustats.longest_time_streak == one_day


@pytest.mark.asyncio
async def test_win_time_active(ustats):

    one_day = 24 * 60 * 60

    date = (dt.datetime.today() - dt.timedelta(days=2)).timestamp()
    await ustats.update_win(date)
    await ustats.end_win_time(date + one_day)
    assert ustats.longest_time_streak == one_day

    date = (dt.datetime.today() - dt.timedelta(hours=6)).timestamp()
    await ustats.update_win(date)

    t = one_day + (6 * 60 * 60)
    assert ustats.total_win_time == pytest.approx(t)
    assert ustats.longest_time_streak == one_day
