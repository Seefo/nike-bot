# flake8: noqa
import discord.ext.test as dpytest
import pytest


@pytest.mark.skip("cannot get this working at this time due to is owner checks")
@pytest.mark.asyncio
async def test_get_config(bot):
    dpytest.configure(bot)
    guild = bot.guilds[0]
    await dpytest.message("!get-nike-config")
    dpytest.verify_message("[Expected help output]")
