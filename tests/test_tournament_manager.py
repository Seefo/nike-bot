import datetime as dt
import random
import string

from itertools import chain

import pytest

from nike_bot.exceptions import InvalidTournamentState, PlayerOperationError
from nike_bot.tournament_manager import TournamentManager, TournamentType


def player_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return "".join(random.choice(chars) for _ in range(size))


@pytest.fixture
async def tmgr(t_redis):
    t = await TournamentManager.from_redis((1, "TestTournament"), t_redis)
    await t.create(type_=TournamentType.RL_3v3, create_bracket=False)
    yield t
    await t.cancel()


@pytest.fixture
async def tcreated(t_redis):
    t1 = await TournamentManager.from_redis((1, "TestCreate"), t_redis)
    try:
        await t1.create()
    except:  # noqa
        # Force destroy our test tournament
        await t1.chal.destroy()
        await t1.create()
    yield t1
    await t1.cancel()


@pytest.mark.asyncio
async def test_tournament_create(tcreated):

    with pytest.raises(InvalidTournamentState):
        await tcreated.create()

    t2 = await TournamentManager.from_redis((1, "TestCreate"), tcreated.t_redis)
    with pytest.raises(InvalidTournamentState):
        await t2.create()


@pytest.mark.asyncio
async def test_tournament_reset(tcreated):

    players = [player_generator() for x in range(9)]
    for p in players:
        await tcreated.add_player(p)

    team = "HypeCrew"
    players = ["ricky", "bobby", "cal"]

    await tcreated.add_team(team, players)

    await tcreated.start(generate_teams=True)

    info = tcreated.chal._tournament
    assert info["participants_count"] == 4
    assert "participants" in info

    await tcreated.unlock()
    info = await tcreated.chal.info()
    assert info["participants_count"] == 0
    assert "participants" in info

    # Lets recreate teams.
    await tcreated.start(generate_teams=True)
    info = await tcreated.chal.info()
    assert info["participants_count"] == 4
    assert "participants" in info

    # Lets make sure this odd behavior didn't break up
    # premade teams
    for participant in info["participants"]:
        part = participant["participant"]
        if part["display_name"].startswith(team):
            break

    for name in players:
        assert name in part["display_name"]


@pytest.mark.asyncio
async def test_tournament_finish(tmgr):
    time = dt.datetime.now()
    players = [player_generator() for x in range(9)]
    for p in players:
        await tmgr.add_player(p)

    await tmgr.start(generate_teams=True)
    assert tmgr.start_time > time
    await tmgr.finish()
    assert tmgr.finish_time > tmgr.start_time


@pytest.mark.asyncio
async def test_add_remove_player(tmgr):
    players = ["john", "bob"]
    for player in players:
        await tmgr.add_player(player)
        assert player in tmgr.players
    assert len(tmgr.players) == 2

    # Test that its saved
    new_tmgr = await TournamentManager.from_redis((tmgr.gid, tmgr.name), tmgr.t_redis)
    assert len(new_tmgr.players) == 2
    for player in players:
        assert player in new_tmgr.players

    # Test removals
    await tmgr.remove_player(players[0])
    assert len(tmgr.players) == 1
    assert players[1] in tmgr.players


@pytest.mark.asyncio
async def test_add_team_new_players(tmgr):
    # add a team with 3 players
    # check that team is added and has 3 players
    # check that 3 players are in _players, _assigned_players
    team = "HypeSquad"
    players = ["john", "bob", "ross"]

    await tmgr.add_team(team, players)

    assert team in tmgr.teams
    assert tmgr.teams[team] == players

    for p in players:
        assert p in tmgr._assigned
        assert p in tmgr.players


@pytest.mark.asyncio
async def test_add_team_old_players(tmgr):
    # add a team with 3 players already in _player pool
    # check that team is added and has 3 players
    # check that 3 players are in _players, _assigned_players
    team = "HypeCrew"
    players = ["Ricky", "Bobby", "Cal"]

    for p in players:
        await tmgr.add_player(p)
        assert p in tmgr.players

    await tmgr.add_team(team, players)
    assert team in tmgr.teams
    assert tmgr.teams[team] == players

    for p in players:
        assert p in tmgr._assigned
        assert p in tmgr.players
        assert p not in tmgr._unassigned

    new_players = ["john", "bob", "ross"]
    for p in new_players:
        await tmgr.add_player(p)
    teams, incomplete = await tmgr.update_teams()
    assert len(teams) == 2
    assert len(incomplete) == 0
    players = []
    for p in teams.values():
        players.extend(p)
    # assert no duplicates and each unique player we added
    # is assigned to only 1 team
    assert len(set(players)) == 6


@pytest.mark.asyncio
async def test_remove_player(tmgr):

    team = "HypeCrew"
    players = ["Ricky", "Bobby", "Cal"]

    for p in players:
        await tmgr.add_player(p)
    removed = players.pop()
    await tmgr.remove_player(removed)
    for p in players:
        assert p in tmgr.players
    assert removed not in tmgr.players

    players.insert(0, removed)
    await tmgr.add_team(team, players)
    await tmgr.remove_player(players[0])
    assert players[0] not in tmgr._teams[team]
    assert players[0] not in tmgr._unassigned
    assert players[0] not in tmgr._assigned


@pytest.mark.asyncio
async def test_remove_team(tmgr):

    team = "HypeCrew"
    players = ["Ricky", "Bobby", "Cal"]

    await tmgr.add_team(team, players)
    assert team in tmgr.teams

    await tmgr.remove_team(team)

    assert team not in tmgr.teams
    for p in players:
        assert p not in tmgr.players
        assert p not in tmgr._assigned

    with pytest.raises(ValueError):
        await tmgr.remove_team(team)


@pytest.mark.asyncio
async def test_create_teams(tmgr):

    # players is evenly divisible by team size
    players = [player_generator() for x in range(3)]
    for p in players:
        await tmgr.add_player(p)

    teams, incomplete = tmgr._create_teams_from_unassigned()

    assert len(teams) == 1
    assert len(incomplete) == 0
    for _name, team in teams.items():
        for p in players:
            assert p in team


@pytest.mark.parametrize(
    "count, tlen, ilen",
    [(4, 1, 1), (5, 1, 2), (6, 2, 0), (15, 5, 0), (16, 5, 1), (17, 5, 2),],  # noqa
)
@pytest.mark.asyncio
async def test_create_teams_incomplete(tmgr, count, tlen, ilen):
    # not evenly divisible
    players = [player_generator() for x in range(count)]

    for p in players:
        await tmgr.add_player(p)

    teams, incomplete = tmgr._create_teams_from_unassigned()

    assert len(teams) == tlen
    assert len(incomplete) == ilen


@pytest.mark.asyncio
async def test_general_dry_run(tmgr):

    players = [player_generator() for x in range(12)]
    for p in players:
        await tmgr.add_player(p)

    assert not tmgr.is_started
    teams, incomplete = await tmgr.update_teams()
    assert len(teams) == 4
    assert len(tmgr.players) == 12
    assert len(incomplete) == 0
    assert not tmgr.is_started



@pytest.mark.asyncio
async def test_general_team_creation(tmgr):

    players = [player_generator() for x in range(12)]
    for p in players:
        await tmgr.add_player(p)

    teams, incomplete = await tmgr.update_teams()
    assert len(teams) == 4
    assert len(incomplete) == 0

    await tmgr.start()
    with pytest.raises(InvalidTournamentState):
        await tmgr.start()


# Test cases:
# swap player on team with non-existant new player
# swap two players on two different teams
# swap none-team player with another non-team player (error)
# try both arg positions 
@pytest.mark.asyncio
async def test_substitute(tmgr):

    players = [player_generator() for x in range(12)]
    for p in players:
        await tmgr.add_player(p)

    teams, incomplete = await tmgr.update_teams()
    assert len(teams) == 4

    # New player, position 1
    old_player = players[0]
    new_player = "foo"
    p_team = tmgr.find_players_team(old_player)
    await tmgr.swap_players(new_player,old_player)
    assert new_player in tmgr._assigned
    assert new_player in tmgr._teams[p_team]
    assert old_player not in tmgr._assigned
    assert old_player not in tmgr._unassigned

    # new player, position 2
    old_player = "foo"
    new_player = "zap"
    p_team = tmgr.find_players_team(old_player)
    await tmgr.swap_players(old_player, new_player)
    assert new_player in tmgr._assigned
    assert new_player in tmgr._teams[p_team]
    assert old_player not in tmgr._assigned
    assert old_player not in tmgr._unassigned

    # both players on different teams

    p1_team = list(tmgr._teams.keys())[1]
    p2_team = list(tmgr._teams.keys())[2]
    print("teams: ",tmgr._teams)
    p1 = tmgr._teams[p1_team][0]
    p2 = tmgr._teams[p2_team][0]
    await tmgr.swap_players(p1,p2)
    assert p1 in tmgr._assigned
    assert p2 in tmgr._assigned
    assert p1 in tmgr._teams[p2_team]
    assert p2 in tmgr._teams[p1_team]

    # Players not on any teams...exception
    with pytest.raises(PlayerOperationError):
        await tmgr.swap_players("1","2")


