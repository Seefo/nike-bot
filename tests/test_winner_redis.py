import pytest


@pytest.mark.asyncio
async def test_check_timeouts(wt_redis):

    player = (123, 456)
    score = 42
    await wt_redis.add_winner_timeout(player[0], player[1], score)
    f = await wt_redis.check_timeouts(43)
    assert len(f) == 1
    timedout_player = f[0][0]
    assert timedout_player == player
    timedout_score = f[0][1]
    assert timedout_score == score

    f = await wt_redis.check_timeouts(41)
    assert len(f) == 0
