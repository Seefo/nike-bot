import pytest

from nike_bot.tournament_stats import TournamentStats


@pytest.fixture
async def t_stats(t_redis):
    return await TournamentStats.from_redis("foo_guild", t_redis)


@pytest.mark.skip("Failing test, not being used")
@pytest.mark.asyncio
async def test_stats(t_stats):
    stats = await t_stats.stats()
    print(stats)
    assert False  # noqa
