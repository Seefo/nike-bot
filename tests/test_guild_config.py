import pytest

from nike_bot.guild_config import Config, GuildConfig


@pytest.fixture
async def gconfig(gc_redis):
    return await GuildConfig.from_redis("foo_guild", gc_redis)


@pytest.mark.asyncio
async def test_read_update_guild_config(gc_redis):
    empty_config = await gc_redis.read_guild_config("foo_guild")
    assert isinstance(empty_config, dict)
    assert not empty_config

    await gc_redis.update_guild_config("foo_guild", baz="boom")

    config = await gc_redis.read_guild_config("foo_guild")
    assert isinstance(config, dict)
    # we stringify the keys as that's important for expanding keywords
    assert config["baz"] == "boom"


@pytest.mark.asyncio
async def test_create_load(gconfig):
    assert gconfig.loaded is True

    g = GuildConfig("foo_guild")
    assert not g.config
    assert g.loaded is False


@pytest.mark.asyncio
async def test_update(gconfig):

    # from_redis loads from persisted data, ensure its
    # default value
    assert gconfig.winner_role == "Champion"

    await gconfig.update(winner_role="champ")
    assert gconfig.winner_role == "champ"
    assert gconfig["winner_role"] == "champ"

    # Ensure we are persisting updates
    await gconfig.load()
    assert gconfig.winner_role == "champ"
    assert gconfig["winner_role"] == "champ"


@pytest.mark.asyncio
async def test_subscriptable(gconfig):
    # Check that we can do this mechanism for setting
    # ensure it persists a load
    gconfig["winner_timeout"] = 42
    from asyncio import sleep

    await sleep(0.1)
    assert gconfig.winner_timeout == 42

    await gconfig.load()
    assert gconfig.winner_timeout == 42


@pytest.mark.asyncio
async def test_as_dict(gconfig):
    d = gconfig.as_dict()
    assert isinstance(d, dict)
    for key in gconfig._default_config:
        assert key in d


def test_config_equality():
    f = Config(42)
    assert f == 42
    f = Config("42")
    assert f == "42"


def test_type():
    f = Config("42", type_=int)
    assert f == 42


def test_subtype():
    f = Config(["42"], type_=list, subtype=int)
    assert f == [42]

    f = Config({"john": "42"}, type_=dict, subtype=int)
    assert f == {"john": 42}

    f = Config({"john": 42}, type_=dict, subtype=str)
    assert f == {"john": "42"}

    f = Config(["subs", "server stuff", "42"], type_=tuple, subtype=str)
    assert f == ("subs", "server stuff", "42")

    f = Config("Mods", type_=list, subtype=str)
    print(f.value)
    assert f.value == ["Mods"]
