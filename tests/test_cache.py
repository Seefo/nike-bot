import pytest

from nike_bot.cache import guild_cache
from nike_bot.guild_config import GuildConfig


# TODO: Generalize this test case.
@pytest.mark.asyncio
async def test_cache_hit():
    f = await guild_cache["foo"]
    assert isinstance(f, GuildConfig)

    b = await guild_cache["foo"]
    assert isinstance(b, GuildConfig)
    assert f is b
