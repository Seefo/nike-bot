import asyncio

import pytest


# import discord.ext.test as dpytest


@pytest.fixture(scope="session")
def event_loop():
    """Change event_loop fixture to module level."""
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture()
async def gc_redis():
    from nike_bot.redis import GuildConfigRedis

    r = GuildConfigRedis()
    await r.connect()
    yield r
    await r.redis.flushdb()
    await r.wait_closed()


@pytest.fixture()
async def us_redis():
    from nike_bot.redis import UserStatsRedis

    r = UserStatsRedis()
    await r.connect()
    yield r
    await r.redis.flushdb()
    await r.wait_closed()


@pytest.fixture()
async def wt_redis():
    from nike_bot.redis import WinTimerRedis

    r = WinTimerRedis()
    await r.connect()
    yield r
    await r.redis.flushdb()
    await r.wait_closed()


@pytest.fixture()
async def t_redis():
    from nike_bot.redis import TournamentRedis

    r = TournamentRedis()
    await r.connect()
    yield r
    await r.redis.flushdb()
    await r.wait_closed()


# @pytest.fixture(scope="session")
# async def bot():
#    bot = commands.Bot(command_prefix='!')
#    for ext in get_exts():
#        bot.load_extension(ext)
#    return bot
