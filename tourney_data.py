import json
import logging
import os
import pprint
import re

import challonge
import discord
import uvloop

from discord.ext import commands

from nike_bot.chal import Chal
from nike_bot.config import env
from nike_bot.utils import async_wrap


uvloop.install()

# Pull all tournaments from Seefo's account
# parse all participants from tournament
# parse winner
# if name not in discord, check dictionary
# Barf on any unknown names
#

bot = commands.Bot(command_prefix="!")

logger = logging.getLogger(__name__)

TOURNEYS_FILE = "tourneys.json"
commands = []
name_map = {
    "rebel": ["misterrebel"],
    "bega": "begawkology",
    "xeph": "xephium",
    "bran": "b.",
    "prod": "",
    "theprod": "",
    "prodigy": "",
    "theprodigy1": "",
    "legend": "alegendv1",
    "insta": "instamole",
    "charles": "",
    # "Too Hot" : [],
    "too hot": "A.",
    "icy": "A.",
    "too icy": "A.",
    "optimus": "op optimus",
    "op": "op optimus",
    "xak": "xakuvo",
    "razor": "razorclad",
    "weese": "weesesnaw",
    "lash": "lashxx",
    "chaotic": "ch4otic bane",
    "tyler": "tyler alvarez-badfish",
    "sergio": "segio",
    "rocker": "rocker4life98",
    "rocker's girl": "",
    "ant": "antlowery",
    "gharbage": "gahbage",
    "machu rocker too hot": ["machu", "rocker4life98", "A."],
    "swabbie lash nickaf": ["swabbie", "lashxx", "nickaf"],
    "bega insta cobra": ["begawkology", "instamole", "papadoc"],
    "thicc-n-tasty chaotic owl": ["T h i c c - N - T a s t y", "ch4otic bane", "owl"],
    "rebel teddy xephium": ["misterrebel", "teddy", "xephium"],
    "botch jbomb sexymanlyman": ["botch", "jbomb"],
    "2dwaffle jmac synthetic": ["2D Waffle™", "jmac HD", "synthetic"],
    "psycho": "",
    "warp": "",
    "waffle": "2D Waffle™",
    "chrome": "",
    "bigice": "bigice",
    "bigice457": "bigice",
    "jmac": "Jmac hd",
    "cookie": "",
    "ketts": "SCRT ketts",
    "squizzle": "dasquizzle",
    "stealth star": "",
    "ihavebananaforu": "sammyboy23",
    "zero2": "dm4sho",
    "zero": "dm4sho",
    "tdy": "ᵗᵈʸ",
    "bigicy": "bigice",
    "synethic": "synthetic",
    "trptman": "",
    "k": "kntihasdbfh",
    "kingmann": "kingmaan",
    "hittin": "hittin1",
    "b": "b.",
    "offandon": "offandongamer",
    "rage": "ragefam",
    "2d waffle": "2D Waffle™",
    "stealth": "",
    "rashford": "",
    "deity": "deitylink",
    "lexie": "lexieloo",
    "cupofhottea": "",
    "elite": "eliteeliminator",
    "ooferefo": "OoferefoO",
    "hiro": "hirostream",
    "star": "",
    "oppie": "opulencelive",
    "dlchamp": "",
    "u4me": "",
    "u4me2015": "",
    "oof": "OoferefoO",
    "nachos": "American Nacho",
    "hound": "houndofspace",
    "stonertortiose": "stonertortoise",
    "kingman": "kingmaan",
    "stoner": "stonertortoise",
    "squiz": "dasquizzle",
    "oreo": "It'sBrittanyBish",
    "razen": "razendorph",
    "skyzer0": "chris",
    "shank": "shankster0095",
    "skater": "",
    "nacho": "American Nacho",
    "guitair": "",
    "kobaltlightning": "",
    "opulence": "opulencelive",
    "zylia": "",
    "nick304": "",
    "sam.": "sammyboy23",
    "yas_drags": "",
    "salomaya": "",
    "salmoya": "",
    "squizz": "DaSquizzle",
    "dasquizz": "DaSquizzle",
    "eric": "ericxcbs",
    "jebus": "",
    "dongerlord": "thedongerlord",
    "sub": "",
    "gt": "",
    "too---icy": "A.",
}
COMMANDS_FILE = "commands.json"


@async_wrap
def get_tournaments():
    return challonge.tournaments.index()


def twos_tourney(teams):
    total_len = 0
    for i, v in enumerate(list(teams.values()), 1):
        total_len += len(v)
    print("i: ", i)
    print("TOURNEY_TYPE: ", total_len / i)
    if (total_len / i) > 2:
        return False
    return True


def sanitize_names(names, guild):
    parsed_names = []
    logger.info("all names: %s", names)
    for name in names:
        name = name.strip().lower()
        if name in name_map:
            name = name_map[name]
        if not name:
            continue
        if not isinstance(name, list):
            name = [name]
        for n in name:
            n = n.lower()
            logger.info("name: %s", n)
            member = discord.utils.find(lambda x: x.name.lower() == n, guild.members)
            if not member:
                raise ValueError("Couldn't find user: %s", n)
            parsed_names.append(member.name)
    # convert to set temporarily to get rid of duplicates, then  return as list
    return list(set(parsed_names))


async def parse_tourney(tourney, guild):
    # Get the team name

    logger.info("Parsing Tournament %s", tourney.name)
    raw_teams = await tourney.teams()
    teams = {}
    winners = ""
    for team in raw_teams:
        # Parse the team and members
        team_split = team["display_name"].split(":")
        team_name = team_split[0].lower()
        teams[team_name] = []

        team_members = re.split("/|\+", team_split[1])
        teams[team_name] = sanitize_names(team_members, guild)
        print(teams[team_name])
        # determine if they won
        if team["final_rank"] == 1:
            # Doing this incase there is a bug and multiple winners
            winners += team_name
    if twos_tourney(teams):
        return
    # now we have parsed everything and have a viable tourney
    commands.append(("CREATE_TOURNEY", tourney.name))
    for k, v in teams.items():
        commands.append(("ADD_TEAM", k, v))
    commands.append(("START_TOURNEY", tourney.name,))
    finish_time = tourney._tournament["completed_at"].timestamp()
    commands.append(("FINISH_TOURNEY", tourney.name, winners, finish_time))


async def parse_commands(command, *args):
    pass


class TournamentBackport:
    def __init__(self, guild):
        self.t = None
        self.guild = guild


async def do_create(tourney_name):
    self.t = TournamentManager(tourney_name)
    await self.t.create(create_bracket=False)


async def do_add_team(team_name, players):
    self.t.add_team(team_name, players)


async def start_tourney(tourney_name):
    self.t.start()


async def finish_tourney(tourney_name, winner_team, date):
    finish_date = dt.datetime.fromutctimestamp(date)
    # update the guild's tournament stats
    self.t.finish(finish_date)

    # update the winner's stats
    players = self.t.teams[winner_team]
    # players = args_get_members(players,


@bot.event
async def on_ready():
    logger.info("We have logged in as %s", bot.user)
    guild = discord.utils.get(bot.guilds, name="Seefo")
    # get tourneys
    if os.path.isfile(TOURNEYS_FILE):
        logger.info("Loading from file")
        with open(TOURNEYS_FILE) as f:
            tournaments = json.load(f)
    else:
        logger.info("Getting from API and storing to file")
        tournaments = await get_tournaments()
        print("Tournaments: ", tournaments)
        with open(TOURNEYS_FILE, "w") as f:
            parsed = []
            for t in tournaments:
                t = (t["name"], t["url"], t["id"])
                parsed.append(t)
            json.dump(parsed, f)
        tournaments = parsed

    for data in tournaments:
        logger.info("Data: %s", data)
        name, url, id_ = data
        tourney = Chal(name, url)
        await tourney.load(id_)
        await parse_tourney(tourney, guild)
    pprint.pprint(commands)
    with open(COMMANDS_FILE, "w") as f:
        json.dump(commands, f)

    # Now we parse the file and execute the commands


def main(token):

    logging.basicConfig(level=env.str("LOG_LEVEL"),)
    logging.getLogger("discord").setLevel("INFO")
    logging.getLogger("discord.gateway").setLevel("WARNING")
    logging.getLogger("websockets").setLevel("WARNING")
    logging.getLogger("twitchio.websocket").setLevel("WARNING")

    bot.run(token)


if __name__ == "__main__":
    try:
        token = env.str("DISCORD_BOT_TOKEN")
    except KeyError as e:
        raise KeyError(
            "Please put token in DISCORD_BOT_TOKEN environment variable"
        ) from e
    main(token)
