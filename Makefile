CMD ?= bash
PROD_COMPOSE ?= docker/docker-compose.prod.yml
DEV_COMPOSE ?= docker/docker-compose.yml
CONTAINER_NAME ?= nike-bot
export UID ?= $(shell id -u)
export GID ?= $(shell id -g)
# YIKES: this will just use our most recent tag, even if its not built from that.
# need protection against dirty and/or non-tagged/branched builds, etc.
export TAG := $(shell git describe --tags --abbrev=0)

setup:
	pre-commit install

rebuild build:
	docker compose build

up:
	docker compose up -d

down:
	docker compose down

logs:
	docker compose logs

shell: up
	docker compose exec -u $(UID) nike-bot $(CMD)

root: up
	docker compose exec -u 0 nike-bot $(CMD)

clean:
	rm -rf dist/* build/*

prod:
	docker compose -p nike-bot-prod -f docker/docker-compose.prod.yml up -d

prod-build:
	docker build -t nike-bot-prod:$(TAG) -f docker/Dockerfile .

prod-reqs:
	$(MAKE) shell CMD="pip-compile --upgrade --output-file requirements.txt requirements.in"

prod-restart:
	docker compose -p nike-bot-prod -f docker/docker-compose.prod.yml restart nike-bot

prod-stop:
	docker compose -p nike-bot-prod -f docker/docker-compose.prod.yml down

prod-logs:
	docker compose -p nike-bot-prod -f docker/docker-compose.prod.yml logs -f nike-bot

#prod-build:
#make build && docker tag nike-bot nike-bot-prod && make prod
