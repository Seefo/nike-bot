import os

from pkg_resources import parse_requirements
from setuptools import find_packages, setup


def requirements(file=None):
    if not file:
        if os.environ.get("BRANCH_NAME", None) == "master":
            file = "requirements.txt"
        else:
            file = "requirements.in"
    with open(file) as f:
        reqs = [str(requirement) for requirement in parse_requirements(f)]
    return reqs


def extra_requirements():
    ex = {
        "test": requirements("test-requirements.txt"),
        "docs": [],  # TODO
    }
    return ex


if __name__ == "__main__":
    setup(
        name="nike-bot",
        author="Youssef Gamal",
        packages=find_packages(exclude=["tests"]),
        install_requires=requirements(),
        extras_require=extra_requirements(),
        setup_requires=['setuptools-git-ver'],
        version_config={
                    "template": "{tag}",
                    "dev_template": "{tag}.dev{ccount}+git.{sha}",
                    "dirty_template": "{tag}.dev{ccount}+git.{sha}.dirty",
                },
        license="MIT",
        classifiers=["Programming Language :: Python :: 3", "License :: MIT License"],
    )
