from discord.ext import commands


class UserException(commands.CheckFailure):
    def __init__(self, msg, *args, **kwds):
        self.user_msg = msg
        super().__init__(msg, *args, **kwds)


class InvalidTournamentState(UserException):
    pass


class UnsupportedArgs(UserException):
    pass


class NoActiveTournaments(UserException):
    pass


class NeedTournamentName(UserException):
    pass


class NotPlayerRole(UserException):
    pass


class NotCommandChannel(UserException):
    pass


class MissingResults(UserException):
    pass


class InvalidTeam(UserException):
    pass


class PlayerOperationError(UserException):
    pass

class IncompleteTeamChange(UserException):
    pass
