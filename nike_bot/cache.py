import logging

from cachetools import LRUCache

from .config import env
from .guild_config import GuildConfig
from .tournament_manager import TournamentManager
from .user_stats import UserStats


logger = logging.getLogger(__name__)


class SmartCache(LRUCache):
    def __init__(self, factory, *args, **kwds):
        self.factory = factory
        super().__init__(*args, **kwds)

    async def __getitem__(self, key):
        # There is a probably a better way to make this work
        # but dont want to focus on this too much at the moment
        try:
            return self._Cache__data[key]
        except KeyError:
            return await self.__missing__(key)

    # def __popitem__(self):
    #    self._Cache__data.popitem()

    async def __missing__(self, key):
        logger.debug("Looking up missing key %s using %s", key, self.factory)
        item = await self.factory(key)
        self[key] = item
        return item


guild_cache = SmartCache(GuildConfig.from_redis, env.int("GUILD_CACHE_SIZE", 15))
user_cache = SmartCache(UserStats.from_redis, env.int("USER_CACHE_SIZE", 1500))
tourney_cache = SmartCache(
    TournamentManager.from_redis, env.int("GUILD_CACHE_SIZE", 15 * 200)
)


def clear_caches():
    guild_cache.clear()
    user_cache.clear()
    tourney_cache.clear()
