import datetime as dt
import json
import logging
import time

from pprint import pformat

from nike_bot.redis import UserStatsRedis


logger = logging.getLogger(__name__)


class UserStats:
    def __init__(self, key, us_redis=None):
        self.gid, self.uid = key
        self.us_redis = us_redis

        # load defaults:
        self._from_dict({})

    @classmethod
    async def from_redis(cls, key, us_redis=None):
        if not us_redis:
            us_redis = UserStatsRedis()
            await us_redis.connect()
        else:
            us_redis = us_redis
        new_ = cls(key, us_redis)
        rdict = await us_redis.get_stats(new_.gid, new_.uid)
        new_._from_dict(rdict)
        return new_

    def _from_dict(self, rdict):
        logger.debug("rdict: %s", pformat(rdict))
        self.entries = int(rdict.get("entries", 0))
        self.wins = int(rdict.get("wins", 0))
        self.mvps = int(rdict.get("mvps", 0))
        self.second = int(rdict.get("second", 0))
        self.third = int(rdict.get("third", 0))
        self.start_win_time = float(rdict.get("start_win_time", 0))
        self.current_winner = (
            True if rdict.get("current_winner", "F") == "True" else False
        )
        self.win_dates = json.loads(rdict.get("win_dates", "[]"))
        self.entry_dates = json.loads(rdict.get("entry_dates", "[]"))
        self.mvp_dates = json.loads(rdict.get("mvp_dates", "[]"))
        self._total_win_time = float(rdict.get("total_win_time", 0))
        self._longest_time_streak = float(rdict.get("longest_time_streak", 0))

    async def store(self):
        self.win_dates.sort()
        self.entry_dates.sort()
        self.mvp_dates.sort()

        d = dict(
            entries=self.entries,
            wins=self.wins,
            mvps=self.mvps,
            second=self.second,
            third=self.third,
            start_win_time=self.start_win_time,
            current_winner=str(self.current_winner),
            win_dates=json.dumps(self.win_dates),
            entry_dates=json.dumps(self.entry_dates),
            mvp_dates=json.dumps(self.mvp_dates),
            total_win_time=self._total_win_time,
            longest_time_streak=self._longest_time_streak,
        )
        logger.debug("Storing %s", d)
        await self.us_redis.update_stats(self.gid, self.uid, **d)

    def get_date(self, time):
        date = (
            dt.datetime.utcfromtimestamp(time)
            .replace(second=0, microsecond=0, minute=0, hour=0)
            .timestamp()
        )
        return date

    async def update_win(self, time=None):
        if not time:
            time = (
                dt.datetime.now()
                .replace(hour=23, minute=59, second=59, microsecond=0)
                .timestamp()
            )

        date = self.get_date(time)
        if date in self.win_dates:
            # We already added a win for that date, most likely a duplicate
            return
        logger.info("Updating (%s,%s) win stats at date: %s", self.gid, self.uid, date)
        self.wins += 1

        # If they are not currently a winner, start their timer
        if not self.current_winner:
            self.start_win_time = time

        # Set them as a winner
        self.current_winner = True

        # Add today's date as a tournament win
        self.win_dates.append(date)
        self.entry_dates.append(date)

        # Bump entries
        self.entries += 1
        await self.store()

    async def update_not_win(self, time=None, place=None):

        date = self.get_date(time)
        if date in self.entry_dates:
            return
        self.entry_dates.append(date)
        self.entries += 1

        if place == 2:
            self.second += 1
        elif place == 3:
            self.third += 1

        await self.store()

    async def update_mvp(self, time=None):

        date = self.get_date(time)
        if date in self.mvp_dates:
            # We already added an mvp for this, probably a duplicate
            return

        self.mvp_dates.append(date)
        self.mvps += 1
        await self.store()

    async def end_win_time(self, time=None):
        if not self.current_winner:
            return
        if not time:
            time = time.time()
        logger.info("Ending (%s,%s) user's Win time: %s", self.gid, self.uid, time)
        new_win_time = time - self.start_win_time
        if new_win_time > self._longest_time_streak:

            # Update our stats/book-keeping
            self.longest_time_streak = new_win_time
        self._total_win_time += new_win_time
        self.start_win_time = 0
        self.current_winner = False
        logger.debug(
            "new win time: %s | Total Win Time: %s", new_win_time, self.total_win_time
        )
        await self.store()

    @property
    def total_win_time(self):
        if self.current_winner:
            win_time = time.time() - self.start_win_time
            total = self._total_win_time + win_time
        else:
            total = self._total_win_time
        return total

    @total_win_time.setter
    def total_win_time(self, x):
        self._total_win_time = x

    @property
    def longest_time_streak(self):
        # Longest streak has multiple scenarios:
        # - streak is already set and they are not on a bigger streak currently
        # - streak is already set and current win time is longer than the streak
        # - no streak is set and they are on their first win streak
        # - no streak is set and they are not on a win streak
        if self._longest_time_streak:
            if self.current_winner:
                possible_streak = time.time() - self.start_win_time
                if possible_streak > self._longest_time_streak:
                    return possible_streak
            return self._longest_time_streak
        # if we have no streak info, lets see if they are current on their
        # first win and return the total time as a winner in that case
        return self.total_win_time

    @longest_time_streak.setter
    def longest_time_streak(self, x):
        self._longest_time_streak = x

    def as_dict(self):
        d = dict(
            wins=self.wins,
            mvps=self.mvps,
            entries=self.entries,
            start_win_time=self.start_win_time,
            current_winner=self.current_winner,
            win_dates=self.win_dates,
            mvp_dates=self.mvp_dates,
            total_win_time=self.total_win_time,
            longest_time_streak=self.longest_time_streak,
        )
        return d
