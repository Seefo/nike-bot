import logging
import os
import pkgutil

import uvloop

from discord import Intents
from discord.ext import commands

from . import ext as nike_ext
from .cache import clear_caches
from .config import env


uvloop.install()


intents = Intents.all()
intents.members = True
bot = commands.Bot(command_prefix="!", intents=intents)


logger = logging.getLogger(__name__)


@bot.event
async def on_ready():
    logger.info("We have logged in as %s", bot.user)
    # Setup up our cogs/commands
    for ext in get_exts():
        await bot.load_extension(ext)
    logger.info("Loaded extensions")


@bot.command(name="reload-ext", hidden=True)
@commands.is_owner()
async def reload_ext(ctx):
    clear_caches()
    for ext in get_exts():
        await bot.reload_extension(ext)
    await ctx.send("Reloaded extensions")


def get_exts(package=nike_ext):
    pkgpath = os.path.dirname(nike_ext.__file__)
    exts = []
    for _, name, _ in pkgutil.iter_modules([pkgpath]):
        if name == "templates":
            continue
        exts.append(f"nike_bot.ext.{name}")
    return exts


def main(token):
    # Setup logging
    logging.basicConfig(
        level=env.str("LOG_LEVEL"),
    )
    logging.getLogger("discord").setLevel("INFO")
    logging.getLogger("discord.gateway").setLevel("WARNING")
    logging.getLogger("websockets").setLevel("WARNING")
    logging.getLogger("twitchio.websocket").setLevel("WARNING")

    # gogogogo
    bot.run(token)


if __name__ == "__main__":
    try:
        token = env.str("DISCORD_BOT_TOKEN")
    except KeyError as e:
        raise KeyError(
            "Please put token in DISCORD_BOT_TOKEN environment variable"
        ) from e
    main(token)
