import logging

from .redis import TournamentRedis
from .tournament_manager import TournamentManager


logger = logging.getLogger(__name__)

SAMPLE_SIZE = 16


def exponential_avg(avg, new):
    avg -= avg / SAMPLE_SIZE
    avg += new / SAMPLE_SIZE
    return avg


def average(data_set):
    return sum(data_set) / len(data_set)


# TODO: Add a tournament object
# name, date, participant IDs, duration.
class TournamentStats:
    def __init__(self, key, t_redis=None):
        self.gid = key
        self.t_redis = t_redis
        self.tournaments = []

    @classmethod
    async def from_redis(cls, key, t_redis=None):
        if not t_redis:
            t_redis = TournamentRedis()
            await t_redis.connect()
        else:
            t_redis = t_redis
        new_ = cls(key, t_redis)
        return new_

    def load_tournaments(self):
        pass

    async def update_tournaments(self):
        pass

    # Stats
    async def stats(self):
        tourneys = await self.t_redis.read_tournaments(self.gid, values=True)
        stats = {}
        participants = []
        durations = []
        largest = 0
        for json in tourneys.values():
            tourney = await TournamentManager.from_json(self.gid, json)
            logger.info("tourney: %s", tourney.info())
            players = tourney.players
            participants.extend(players)
            if len(players) > largest:
                largest = len(players)
            durations.append((tourney.finish_time - tourney.start_time).total_seconds())

        if tourneys:
            stats["Average Per Tournament"] = len(participants) / len(tourneys)
            logger.info("durations: %s", durations)
            logger.info("durations: %s", sum(durations))
            stats["Average Duration"] = sum(durations) / len(durations)
            stats["Longest Tournament"] = max(durations)
        else:
            stats["Average Per Tournament"] = 0
            stats["Average Duration"] = 0
            stats["Longest Tournament"] = 0

        stats["Unique Players"] = set(participants)
        stats["Largest Tournament"] = largest
        return stats
