import datetime as dt
import logging
import re

from pprint import pformat
from typing import Dict, List

import challonge

from challonge import ChallongeException

from nike_bot.config import env
from nike_bot.exceptions import MissingResults
from nike_bot.utils import async_wrap


ROCKET_LEAGUE = 37591
ORIG_ID = env.str("CHAL_ORG_ID", "")

logger = logging.getLogger(__name__)


def init_challonge(user=None, token=None):
    if not user:
        user = env.str("CHALLONGE_USER")
    if not token:
        token = env.str("CHALLONGE_TOKEN")
    challonge.set_credentials(user, token)


# Just doing this for now to be consistent with previous behavior
# this should move in the near future
init_challonge()


@async_wrap
def get_tournaments():
    subdomain = "ca62412badb8f7ac75bc4ca7"
    return challonge.tournaments.index(subdomain=subdomain)


class ChallongeTournament:
    def __init__(self, name=None, url=None):
        self._name = name
        self._tid = None
        self._tournament = None

    @property
    def start_time(self):
        return self._tournament["started_at"]

    @property
    def finish_time(self):
        return self._tournament["completed_at"]

    @property
    def participants(self):
        parts = [p["participant"] for p in self._tournament["participants"]]
        return parts

    @property
    def url(self):
        if self._tournament:
            return self._tournament["full_challonge_url"]
        else:
            return None

    @property
    def tid(self):
        if self._tid:
            return self._tid
        else:
            return self.create_url()

    def create_url(self):
        url = "sfg_" + re.sub(r"([-\s])", "_", self.name)
        return url

    @async_wrap
    def load(self, id_=None):
        if id_:
            self._tid = id_

        # NOTE: if id_ is not provided, this won't work. Challonge API doesn't work
        # urls like it says.
        self._tournament = challonge.tournaments.show(self.tid, include_participants=1)
        self._tid = self._tournament["id"]
        self.name = self._tournament["name"]

    @async_wrap
    def generate(self, name):
        self.name = name
        start_at = dt.datetime.today().replace(hour=18, minute=30).isoformat()
        url = self.create_url()
        tournament = challonge.tournaments.create(
            self.name,
            url,
            "double elimination",
            game_id=ROCKET_LEAGUE,
            start_at=start_at,
            organization_id=ORIG_ID,
        )
        logger.debug("Tournament configuration: \n%s", pformat(tournament))
        self._tid = tournament["id"]
        self._tournament = tournament
        return self._tid

    @async_wrap
    def join(self, team_name):
        try:
            return challonge.participants.create(self.tid, team_name)
        except ChallongeException:
            # Probably joined multiple times
            print("TODO: AN EXCEPTION OCCURRED WHILE JOINING")

    @async_wrap
    def bulk_join(self, teams: List[str]) -> List[Dict]:
        try:
            return challonge.participants.bulk_add(self.tid, teams)
        except ChallongeException:
            # TODO:
            raise

    @async_wrap
    def start(self):
        state = challonge.tournaments.start(self.tid, include_participants=1)
        self._tournament = state
        return state

    @async_wrap
    def reset(self):
        state = challonge.tournaments.reset(self.tid, include_participants=1,)

        # Clear out all participants, since we maybe recreating teams
        # TODO: they dont have this api call yet
        # challonge.participants.clear(self.tid)

        for participant in state["participants"]:
            participant = participant["participant"]
            challonge.participants.destroy(state["id"], participant["id"])
        state["participants_count"] = 0
        self._tournament = state
        return state

    @async_wrap
    def destroy(self):
        return challonge.tournaments.destroy(self.tid)

    @async_wrap
    def finish(self):
        if not self._tournament["completed_at"]:
            try:
                state = challonge.tournaments.finalize(self.tid, include_participants=1)
                self._tournament = state
            except ChallongeException as e:
                if "fill in all scores" in e.args[0]:
                    raise MissingResults(
                        f"Cannot determine winners, Fill in all scores: {self.url}"
                    )
                raise
        results = {}
        for participant in self._tournament["participants"]:
            participant = participant["participant"]
            rank = participant["final_rank"]
            logger.debug("Participant: %s", pformat(participant))
            team, players = participant["display_name"].strip().rsplit(":", 1)
            results[rank] = {"team": team, "players": players.strip().split("/")}

        return results

    @async_wrap
    def info(self, include_participants=1, include_matches=0):
        state = challonge.tournaments.show(
            self.tid,
            include_participants=include_participants,
            include_matches=include_matches,
        )
        self._tournament = state
        return state

    @async_wrap
    def matches(self, state_filter="open", user_filter=None):
        return challonge.matches.index(
            self.tid,
            state=state_filter,  # (all|pending|open|complete)
            participant_id=user_filter,
        )

    @async_wrap
    def match(self, id):
        return challonge.matches.show(self.tid, id)

    @async_wrap
    def shuffle(self) -> None:
        return challonge.participants.randomize(self.tid)

    @async_wrap
    def teams(self):
        return challonge.participants.index(self.tid)

    @async_wrap
    def team(self, id):
        return challonge.participants.show(self.tid, id, include_matches=1)
