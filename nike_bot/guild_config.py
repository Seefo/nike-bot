import asyncio
import json
import logging

from collections.abc import Iterable, Mapping, Sequence  # noqa
from copy import copy

# from . import bot
from .audit import audit_log
from .redis import GuildConfigRedis


logger = logging.getLogger(__name__)


class Config:
    """
    Wrapper class around config values. Has a validator callable, for
    ensuring the value matches metadata/typing. Validator should return
    the validated/typed value. Default returns as-is.
    """

    def __init__(self, value, type_=None, subtype=None, validator=lambda x: x):
        self.value = None
        self.validator = validator

        self.type_ = type_

        if subtype:
            if not self.type_:
                raise TypeError("Must have a type to have a subtype")

        self.subtype = subtype

        self.update(value)

    def __eq__(self, b):
        return self.value == b

    def type_value(self, value):
        logger.debug(
            "Config of (T: %s, S: %s) from '%s' to '%s'",
            self.type_,
            self.subtype,
            self.value,
            value,
        )
        if self.subtype:
            if isinstance(value, Mapping):
                # Its a dict like object
                new = {}
                for k, v in value.items():
                    new[k] = self.subtype(v)
                value = new
            elif isinstance(value, (list, tuple)):
                # Its a list or tuple
                value = self.type_([self.subtype(x) for x in value])
            else:
                # Its something that belongs in the main type
                value = self.type_([self.subtype(value)])
        elif self.type_:
            value = self.type_(value)
        else:
            value = value

        return value

    def update(self, value):
        # Validate the value, where validator can massage its type
        # we reassign here to make sure our value has the right typing applied
        # self.value = self.validator(value)
        self.value = self.type_value(value)
        return self.value


def check_channel(*args, **kwds):
    pass


def check_roles(*args, **kwds):
    pass


# def check_channel(chan, guild):
#    return find(lambda x: x.name.lower() == chan.lower(), guild.channels)
#
#
# def check_roles(c):
#    found = []
#    for role in roles:
#        found.append(bool(find(lambda x: x.name.lower() == role.lower(), guild.roles)))
#    return all(found)


class GuildConfig:
    _default_config = {
        # Management/admin related
        "manager_roles": Config([], type_=list, subtype=str),
        "allow_mods": Config(True, type_=bool),
        "winner_role": Config("Champion", type_=str, validator=check_roles),
        "winner_timeout": Config(
            (60 * 60 * 24 * 15), type_=int
        ),  # last multiply is # of days
        # Configurations around bot announcing things
        "announce_channel": Config("test-chan", type_=str),
        "announce_create": Config("Tourney Created", type_=str),
        "announce_winner": Config("Hi Congrats", type_=str),
        # restrict player join/leave tournament commands to a specific channel
        "tourney_channel": Config([], type_=list, subtype=str, validator=check_channel),
        # restrict bot to player_commands_channel for ALL commands
        "restrict_bot_listen": Config(False, type_=bool, validator=check_channel),
        # Restrict join/team commands to this role
        "player_role": Config([], type_=list, subtype=str, validator=check_roles),
    }

    def __init__(self, gid, gc_redis=None):
        # Since we use our setattr/getattr to update this dict, its important
        # its initialized before anything else
        self.config = dict()
        # TODO: Fix this...just directly accept guild all the time?
        self.gid = gid
        # self.guild = bot.get_guild(self.gid)
        self.loaded = False
        self.gc_redis = gc_redis

    @classmethod
    async def from_redis(cls, key, gc_redis=None):
        if not gc_redis:
            gc_redis = GuildConfigRedis()
            await gc_redis.connect()
        else:
            gc_redis = gc_redis
        new_ = cls(key, gc_redis)
        return await new_.load()

    def validate(self, **params):
        # Override defaults with our parameters
        validated = self.config
        logger.debug("Original config: %s", self.config)
        for param in params:
            if param not in self._default_config:
                logger.warning(
                    f"Skipping unknown parameter {param}. are you sure its not a typo?"
                )
                continue
            config = copy(self._default_config[param])
            config.value = config.update(params[param])
            validated[param] = config
        logger.debug("Updated config: %s", self.config)
        return validated

    async def load(self):
        config = await self.gc_redis.read_guild_config(self.gid)
        config = {k: json.loads(v) for k, v in config.items()}
        self.validate(**config)
        self.loaded = True
        logger.info("Got this config from redis: %s", config)
        return self

    async def update(self, **params):
        audit_log.info("Attempting to update config with: %s", params)
        self.validate(**params)

        # Need to construct a raw type dict
        for_storage = {}
        for k, v in self.config.items():
            for_storage[k] = json.dumps(v.value)
        if for_storage:
            logger.info("Updating stored config for %s to %s", self.gid, for_storage)
            await self.gc_redis.update_guild_config(self.gid, **for_storage)

    def as_dict(self):
        formatted = {}
        for key in self._default_config:
            formatted[key] = getattr(self, key)
        return formatted

    def __getitem__(self, attr):
        return self.__getattr__(attr)

    def __setitem__(self, attr, value):
        self.__setattr__(attr, value)

    def __getattr__(self, attr):
        if not self.loaded:
            raise ValueError("trying to read config without loading it first!")
        if attr in self.config:
            return self.config[attr].value
        else:
            return self._default_config[attr].value

    def __setattr__(self, attr, value):
        """
        Updates local instance and adds an async step to update this
        """
        if attr in self._default_config:
            kwd = {attr: value}
            self.validate(**kwd)
            asyncio.ensure_future(self.update(**kwd))
        else:
            super().__setattr__(attr, value)
