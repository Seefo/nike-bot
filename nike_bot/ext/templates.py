from jinja2 import Template


# TODO: add all templates into a cog so they can reloaded without taking down the bot
# class NikeTemplates(commands.Cog):
guild_config_tmpl = Template(
    """\
```
{%- for key,value in configs.items() | sort %}
{{ key.ljust(27) }}:{{value}}
{%- endfor %}
```
"""
)


announce_winner = Template(
    """\
**
{%- for winner in winners | sort %}
{{ winner }}
{%- endfor %}
**
"""
)

user_stat_tmpl = Template(
    """\
```
{{ "%-35s" | format("Total Entries") }}| {{ entries }}
{{ "%-35s" | format("Top 3 Finishes") }}| {{ top_finishes  }} ({{top_perc}})
{{ "%-35s" | format("Days from last win") }}| {{ last_win }}

{{ "%-35s" | format("Wins") }}| {{ wins }}
{{ win_dates }}
{{ "%-35s" | format("MVPs") }}| {{ mvps }}
{{ mvp_dates }}
{{ "%-35s" | format(role) }}| {{ win_time }}
{{ "%-35s" | format("Longest Time Streak")}}| {{ time_streak}}
```
"""
)


tourney_tmpl = Template(
    """\
**\|\|\| {{ name }} \|\|\|**
**Players**
{{ players}}
**Status**
{{config}}


"""  # noqa: W605
)

tourney_teams_tmpl = Template(
    """\
{%- for k,v in teams.items() | sort %}
{{ k }} : {{ v | join('/')}}
{%- endfor%}
"""
)

twitch_tourney_info = Template(
    """\
{{ name}} ( {{ player_count }} players)\n
"""
)

twitch_help = """\
Tournaments are semi-automated now! To join run: ?player-join <discord_name>
If you have spaces in your name, please quote it. If you have the same name in
discord, then don't put anything! To see active tournaments do ?tourneys
"""

twitch_multi_help = """\
If multiple tournaments are on-going (rare), you can join by doing:
?player-join <discord> <tournament name>
You must include your discord name before the tournament name in this case.
Same rules apply, make sure you quote anything that has spaces.
"""


def setup(bot):
    pass
    # bot.add_cog(NikeTemplates(bot))
