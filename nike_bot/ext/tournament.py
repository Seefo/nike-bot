import asyncio
import logging
import random
import time

import discord

from discord import Embed
from discord.ext import commands as commands
from discord.ext import tasks
from discord.utils import find

from nike_bot.cache import guild_cache, tourney_cache, user_cache
from nike_bot.config import env
from nike_bot.exceptions import (
    InvalidTournamentState,
    NeedTournamentName,
    NoActiveTournaments,
    UnsupportedArgs,
)
from nike_bot.exceptions import UserException as NikeUserException
from nike_bot.redis import TournamentRedis, WinTimerRedis
from nike_bot.utils import args_get_members, has_manager_perms, has_player_role

# from .nike_twitch import TwitchTournament
from .templates import announce_winner, tourney_teams_tmpl  # tourney_tmpl,


logger = logging.getLogger(__name__)


EMOJI_LIST = [
    # Custom emojis
    "<:grand_plat:716517397426601994>",
    # "<:seefogHype:717122712312479763>",
    "<a:bongocat:719420034950365184>",
    "<a:aaw_yeah:719422006507995206>",
    "<a:blob100:719422006608527410>",
    "<a:meow_party:719422008508809216>",
    "<a:poopfire:719422009578094622>",
    # "<:seefogToxic:721109011180552213>",
    # "<:seefogSalt:725231410381848627>",
    "<:grand_eggplant:725398061169246288>",
    "<:grand_champ:725404073938911332>",
    "<a:homerhide:725406161397874772>",
    "<a:partyparrot:725406261826551879>",
]


class TournamentManager(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        # self.twitch_bot = TwitchTournament(bot)
        self.t_redis = TournamentRedis()

        # logger.info("Starting Twitch Integration")
        # self.bot.loop.create_task(self.twitch_bot.start())

        self.win_timer = WinTimerRedis()
        if env.bool("TIMEOUT_RUN", True):
            logger.info("Running with the Timeout task enabled")
            self.timeout_winners.start()

    async def cog_before_invoke(self, ctx):
        await self.t_redis.connect()

    @commands.command(name="tourney")
    async def tournament_info(self, ctx, name):
        tourney = await tourney_cache[(ctx.guild.id, name)]
        if tourney.exists:
            await ctx.send(embed=self.get_tourney_embed(tourney))
        else:
            await ctx.send("No tournament exists with that name!")

    @commands.command(name="tourneys")
    async def tournaments_info(self, ctx):
        """
        Finds all active tournaments in this server and print their info.
        """
        gid = ctx.guild.id

        tourneys = await self.get_tournaments(gid)
        if tourneys:
            await ctx.send("Currently active tournaments:\n")
            for name in tourneys:
                tourney = await tourney_cache[(gid, name)]
                embed = self.get_tourney_embed(tourney)
                await ctx.send(embed=embed)
        else:
            await ctx.send("No currently active Tournaments!")

    @commands.command(
        name="create-tourney",
    )
    @has_manager_perms(guild_cache)
    async def create_tournament(self, ctx, tourney_name, *, args=None):
        """
        Manager interface for creating tournaments. Requires a tournament name (use
        quotes if it has spaces).
        ex.
            !create-tourney "Seefo RL Test"
        """
        # Other args could be tournament metadata, but not sure yet
        if args:
            raise UnsupportedArgs("Non-default Tournaments are not supported yet!")

        gid = ctx.guild.id
        tournament = await tourney_cache[(gid, tourney_name)]
        await tournament.create()
        await ctx.send(f"Created {tourney_name} Tournament.")

        # make the guild announcement
        gconfig = await guild_cache[gid]
        channel = find(lambda x: x.name == gconfig.announce_channel, ctx.guild.channels)
        await channel.send(f"{gconfig.announce_create}")
        await channel.send(embed=self.get_tourney_embed(tournament))

    @commands.command(
        name="create-teams",
    )
    @has_manager_perms(guild_cache)
    async def create_tourney_teams(self, ctx, tourney_name):
        """
        Kick off the random pool team creation step. Rerunnable if the teams are
        un-even. will list ALL teams, including ones added manually. Teams won't be
        added to the bracket.
        ex.
            !create-teams "Seefo RL Test"
        """
        gid = ctx.guild.id

        tournament = await tourney_cache[(gid, tourney_name)]
        teams, incomplete = await tournament.update_teams()
        team_str = tourney_teams_tmpl.render(teams=teams)
        if incomplete:
            team_str += f"\n\n**INCOMPLETE TEAM** : {incomplete}"
        await ctx.send(f"Here are the teams:\n {team_str}")

    @commands.command(
        name="start-tourney",
    )
    @has_manager_perms(guild_cache)
    async def start_tournament(self, ctx, tourney_name):
        """
        Manager interface for starting the tournament after all players have joined.
        This will lock the tournament in discord and add all teams to the bracket
        Make sure you run this after !create-teams has ran, otherwise the random pool
        won't be used.
        ex.
            !start-tourney "Seefo RL Test"
        """
        gid = ctx.guild.id
        logger.info("Starting tournament (%s,%s) with bracket", gid, tourney_name)
        tournament = await tourney_cache[(gid, tourney_name)]
        await tournament.start()
        await ctx.send(
            "Tournament is locked and ready, Info:",
            embed=self.get_tourney_embed(tournament),
        )

    @commands.command(
        name="finish-tourney",
    )
    @has_manager_perms(guild_cache)
    async def finish_tournament(self, ctx, tourney_name, mvp_player=None):
        """
        Manager interface for finishing the tournament and announcing winners.
        ex.
            !finish-tourney "Seefo RL Test" "Team 42"
        """
        gid = ctx.guild.id

        tournament = await tourney_cache[(gid, tourney_name)]

        if mvp_player:
            mvp_player = args_get_members(mvp_player, ctx.guild.members)[0]
            if mvp_player.name not in tournament.players:
                await ctx.send(
                    f"{ctx.author.mention} player {mvp_player.name} is not in"
                    "tournament!"
                )
                return

        results = (
            await tournament.finish()
        )  # Lets do our book-keeping first, then announce.
        winners = results.pop(1)
        tdate = time.time()

        # Do first place stuff
        try:
            players = args_get_members(winners["players"], ctx.guild.members)
        except ValueError:
            logger.exception("Failed to get members: ")
            await ctx.send(
                "Unexpected error finding Discord Members. Please run "
                "the winner command manually"
            )
        else:
            logger.info("Tournament %s finished. Winners are %s", tourney_name, players)
            await self.winner_logic(
                ctx, players, tdate=tdate, team_name=winners["team"]
            )

        # Do MVP Stuff
        if mvp_player:
            win_stats = await user_cache[(ctx.guild.id, mvp_player.id)]
            await win_stats.update_mvp(tdate)

        # Do other place stuff
        for place, info in results.items():
            players = args_get_members(info["players"], ctx.guild.members)
            for player in players:
                win_stats = await user_cache[(ctx.guild.id, player.id)]
                await win_stats.update_not_win(tdate, place)

    @commands.command(name="cancel-tourney")
    @has_manager_perms(guild_cache)
    async def cancel_tournament(self, ctx, tourney_name):
        """
        Cancels a currently active tournament. Tournament will be deleted entirely.
        Another tournament can be created with the same name.
        ex.
            !cancel-tourney "Seefo RL Test"
        """

        key = (ctx.guild.id, tourney_name)
        tournament = await tourney_cache[key]
        if tournament.is_active:
            await tournament.cancel()
            tourney_cache.pop(key)
            await ctx.send("Cancelled tournament")
        else:
            await ctx.send("Tournament doesn't exist")

    def find_tournament_in_list(self, names, tourney_list):
        """
        Finds the first tournament in a list, if any. Will pop from the list to
        sanitize player names from tournaments in multi-argument commands
        """
        logger.info("Names: %s", names)
        logger.info("Names: %s", tourney_list)
        try:
            ind = next(ind for ind, x in enumerate(names) if x in tourney_list)
            name = names.pop(ind)
        except StopIteration:
            name = None
        return name

    @commands.command(
        name="add-players",
    )
    @has_manager_perms(guild_cache)
    async def add_players(self, ctx, *players):
        """
        Manager interface for adding players to tournaments. Default adds the player to
        the currently active tournament. If there are more than one tournaments, it
        will require the tournament name as the second argument
        ex.
            !add-player  <tournament name> "Johnny Boi" "Billy Blob" Sally th3hunt3r
        """
        await self.admin_member_modify(ctx, False, players)

    @commands.command(
        name="remove-players",
    )
    @has_manager_perms(guild_cache)
    async def remove_players(self, ctx, *players):
        """
        Manager interface for removing players from tournaments. Default applies the
        player to the currently active tournament. If there are more than one
        tournaments, it will require the tournament name as the second argument
        ex.
            !remove-player "Johnny Boi" [tournament name]
        """
        # Dont check discord, we could be removing a twitch player
        await self.admin_member_modify(ctx, True, players)

    async def admin_member_modify(self, ctx, remove, players):
        gid = ctx.guild.id
        players = list(players)
        try:
            tourney_name = await self.get_tournament_name(gid)
        except NeedTournamentName:
            tourneys = await self.get_tournaments(gid)
            tourney_name = self.find_tournament_in_list(players, tourneys)
            if not tourney_name:
                raise

        get_member = commands.MemberConverter()
        for player in players:
            player = await get_member.convert(ctx, player)
            logger.debug("Remove: %s on player %s", remove, player)
            await self.update_player(gid, player, tourney_name, remove=remove)
        if remove:
            await ctx.send(f"Removed {players} from Tournament {tourney_name}")
        else:
            await ctx.send(f"Added {players} to Tournament {tourney_name}")

    @commands.command(name="swap-players")
    @has_manager_perms(guild_cache)
    async def swap_players(self, ctx, player1: discord.Member, player2: discord.Member):
        gid = ctx.guild.id

        tourney_name = await self.get_tournament_name(gid)
        tourney = await tourney_cache[(gid, tourney_name)]

        await tourney.swap_players(player1.name, player2.name)
        team1 = tourney.find_players_team(player1.name)
        team2 = tourney.find_players_team(player2.name)
        await ctx.send(
            f"{player1.name} to team '{team1}' /// {player2.name} to team '{team2}'"
        )

    @commands.command(name="add-to-team")
    @has_manager_perms(guild_cache)
    async def add_to_team(self, ctx, team, player, tourney_name=""):
        pass

    @commands.command(name="remove-from-team")
    @has_manager_perms(guild_cache)
    async def remove_from_team(self, ctx, team, player, tourney_name=""):
        pass

    ##############################
    # Individual player commands #
    ##############################
    @commands.command(name="player-join")
    @has_player_role(guild_cache)
    # @is_in_command_channel()
    async def join_tournament(self, ctx, tourney_name=""):
        gid = ctx.guild.id
        if tourney_name == ctx.author.name:
            # If JohnnyB does !player-join JohnnyB, we should
            # ignore his name here and look for a tournament anyway
            # people have a habit of doing this.
            logger.info(
                "Ignoring provided %s name, since its the author's name", tourney_name
            )
            tourney_name = None
        if not tourney_name:
            tourney_name = await self.get_tournament_name(gid)
        try:
            await self.update_player(gid, ctx.author.name, tourney_name, remove=False)
            await ctx.send(f"You're in, {ctx.author.mention}. Good luck!")
        except InvalidTournamentState as e:
            await ctx.send(e.user_msg)

    @commands.command(name="player-leave")
    @has_player_role(guild_cache)
    # @is_in_command_channel()
    async def leave_tournament(self, ctx, tourney_name=None):
        gid = ctx.guild.id
        if not tourney_name:
            tourney_name = await self.get_tournament_name(gid)

        try:
            await self.update_player(gid, ctx.author.name, tourney_name, remove=True)
            await ctx.send(f"<@{ctx.author.id}> left {tourney_name}.")
        except InvalidTournamentState as e:
            await ctx.send(e.user_msg)

    @commands.command(name="join-team")
    @has_player_role(guild_cache)
    async def join_team(self, ctx, team_name):
        pass

    @commands.command(name="leave-team")
    @has_player_role(guild_cache)
    async def leave_team(self, ctx, team_name):
        pass

    #################
    # Team commands #
    #################
    @commands.command(name="change-team-name")
    async def change_team_name(self, ctx, new_name, tourney_name=None):
        gid = ctx.guild.id
        if not tourney_name:
            tourney_name = await self.get_tournament_name(gid)

        tourney = await tourney_cache[(gid, tourney_name)]
        if not tourney.is_open:
            await ctx.send(f"No tourney by the name '{tourney_name}' was found!")
            return
        try:
            team_name = tourney.find_players_team(ctx.author.name)
            await tourney.change_team_name(team_name, new_name)
        except KeyError:
            await ctx.send("Player not on a team… Are teams created yet?")
        else:
            await ctx.send(f"Changed Team '{team_name}' to '{new_name}'")

    @commands.command(name="team-join")
    async def team_join(self, ctx, team_name, *players):
        if not players:
            ctx.send("Please provide a list of players to add to the team!")
        gid = ctx.guild.id
        players = list(players)  # make it mutable
        try:
            tourney_name = await self.get_tournament_name(gid)
        except NeedTournamentName:
            tourneys = await self.get_tournaments(gid)
            tourney_name = self.find_tournament_in_list(players, tourneys)
            if not tourney_name:
                raise

        players = args_get_members(players, ctx.guild.members)

        await self.update_team(gid, team_name, tourney_name, players, remove=False)
        await ctx.send(
            f"{ctx.author.mention} added team '{team_name}' to the tournament!"
        )

    @commands.command(name="team-leave")
    async def team_leave(self, ctx, team_name, tourney_name=None):
        gid = ctx.guild.id
        if not tourney_name:
            tourney_name = await self.get_tournament_name(gid)

        await self.update_team(gid, team_name, tourney_name, remove=True)
        await ctx.send(f"Removed {team_name} to the tournament!")

    def get_tourney_embed(self, tourney):
        tourney.info()
        embed = Embed(
            title=tourney.name,
            url=tourney.bracket if tourney.bracket else None,
            colour=0xDC143C if tourney.is_started else 0x32CD32,
        )
        players = ", ".join(sorted(tourney.players)) if tourney.players else "None"
        if tourney.teams:
            teams = "\n".join(
                [f"{k}: {', '.join(v)}" for k, v in tourney.teams.items()]
            )
        else:
            teams = "Teams haven't been generated yet."

        embed.add_field(
            name=f"Players ({len(tourney.players)})",
            value=players,
            inline=False,
        )
        embed.add_field(name="Teams", value=teams, inline=False)
        embed.add_field(name="Bracket", value=tourney.bracket, inline=False)
        return embed

    async def update_player(self, gid, player, tourney_name, remove=False):
        key = (gid, tourney_name)
        tournament = await tourney_cache[key]

        if isinstance(player, str):
            player = player
        else:
            # Assume its Discord's Member class
            player = player.name

        if remove:
            await tournament.remove_player(player)
        else:
            await tournament.add_player(player)

    async def update_team(self, gid, team, tourney_name, players=None, remove=False):
        key = (gid, tourney_name)
        tournament = await tourney_cache[key]
        if remove:
            await tournament.remove_team(team)
        else:
            p = []
            for player in players:
                if isinstance(player, str):
                    player = player
                else:
                    # Assume its Discord's Member class
                    player = player.name
                p.append(player)

            await tournament.add_team(team, p)

    async def get_tournaments(self, gid):
        await self.t_redis.connect()  # yikes...
        tourneys = await self.t_redis.read_active_tournaments(gid)
        return tourneys

    async def get_tournament_name(self, gid):
        tourneys = await self.get_tournaments(gid)

        if len(tourneys) == 0:
            raise NoActiveTournaments("No active tournaments for this channel!")
        elif len(tourneys) > 1:
            raise NeedTournamentName(
                f"Found multiple tournaments, please specify between {tourneys}"
            )

        tourney = tourneys[0]
        return tourney

    async def winner_logic(self, ctx, winners, tdate=None, team_name="these players"):
        logger.debug("doing winner logic")

        await self.win_timer.connect()
        win_list = [w.name for w in winners]
        # get guild Config
        gconfig = await guild_cache[ctx.guild.id]

        # get role based on config
        role = find(lambda x: x.name == gconfig.winner_role, ctx.guild.roles)
        if not role:
            ctx.send(f"Role {gconfig.winner_role} could not be found in the guild!")
            return
        # Gets us today's date in seconds:
        if not tdate:
            tdate = time.time()

        # get Timeout
        timeout = int(tdate + gconfig.winner_timeout)

        logger.info("Adding winners with timeout: %s", gconfig.winner_timeout)
        for winner in winners:
            await self.win_timer.add_winner_timeout(ctx.guild.id, winner.id, timeout)
            await winner.add_roles(role)
            win_stats = await user_cache[(ctx.guild.id, winner.id)]
            await win_stats.update_win(tdate)

        await ctx.send(f"Gave '{win_list}' the '{role.name}' role")

        # Now lets make the announcement
        await self.announce_winners(gconfig, ctx, win_list)

    async def announce_winners(self, gconfig, ctx, win_list):
        channel = find(lambda x: x.name == gconfig.announce_channel, ctx.guild.channels)
        win_str = announce_winner.render(winners=win_list)
        msg = await channel.send(f"{gconfig.announce_winner}\n{win_str}")
        try:
            # await all reactions together, a bit of a perf. bump
            reacts = [msg.add_reaction(emj) for emj in random.sample(EMOJI_LIST, 3)]
            await asyncio.gather(*reacts)
        except BaseException:
            pass

    def get_win_timer(self):
        """
        In some scenarios a tournament may end later than normal. This could lead to
        players losing their Winner Role purely due to timing, rather than not winning
        within the "timeout period". To avoid this, we give people a grace period
        where the counter is not necessarily running yet.
        """
        pass

    @tasks.loop(seconds=env.int("TIMEOUT_CHECK_PERIOD", 120))
    async def timeout_winners(self):
        now = time.time()
        await self.win_timer.connect()
        timeouts = await self.win_timer.check_timeouts(now)

        if timeouts:
            logger.info("At Score %s, found timeouts: %s", now, timeouts)
            for key, score in timeouts:
                gid, uid = key
                guild = self.bot.get_guild(gid)
                if not guild:
                    guild = await self.bot.fetch_guild(gid)
                    if not guild:
                        logger.warning("Bot no longer in this guild? %s", gid)
                        continue
                gconfig = await guild_cache[gid]
                # Skip if role is deleted
                role = find(lambda x: x.name == gconfig.winner_role, guild.roles)
                if not role:
                    logger.warning(
                        "No %s role found in guild's list of roles: %s",
                        role,
                        guild.roles,
                    )
                    # Skip any deletes/whatever
                    continue
                # Skip if user is gone
                user = guild.get_member(uid)
                if not user:
                    try:
                        user = await guild.fetch_member(uid)
                    except discord.NotFound:
                        user = self.bot.get_user(uid)
                        logger.warning("User is no longer in guild? %s", user)
                        continue
                logger.debug("Timeout found guild: %s, user: %s", guild, user)
                # skip if this fails for some reason
                await user.remove_roles(role)
                user_stats = await user_cache[(gid, uid)]
                await user_stats.end_win_time(score)

            await self.win_timer.delete_timeouts(now)

    async def cog_command_error(self, ctx, error):
        sendable_errors = (NikeUserException,)

        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send(error)
        if isinstance(error, commands.CheckFailure):
            await ctx.send(error.user_msg)
        elif isinstance(error, commands.CommandInvokeError):
            orig = error.original
            if isinstance(orig, sendable_errors):
                await ctx.send(orig.user_msg)
            else:
                raise orig
                # logger.error("Unknown exception: %s",error)
        else:
            raise error
            # logger.error("Unknown exception: %s",error)


async def setup(bot):
    logger.info("Setting up Tournament Interface")
    await bot.add_cog(TournamentManager(bot))
