import datetime as dt
import logging

from datetime import datetime, timedelta

from discord.ext import commands

from nike_bot.cache import guild_cache, user_cache
from nike_bot.ext.templates import user_stat_tmpl
from nike_bot.utils import (  # , has_manager_perms
    args_get_members,
    get_dd_hh_mm_ss,
)


logger = logging.getLogger(__name__)


class UserStats(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        name="stats",
        description="Show your Tournament stats",
        help="Show your Tournament stats",
    )
    async def my_stats(self, ctx):
        uid = ctx.message.author.id
        gid = ctx.guild.id
        resp = await self.stats(gid, uid)

        await ctx.send(resp)

    @commands.command(name="user-stats", hidden=True)
    @commands.is_owner()
    async def user_stats(self, ctx, *user):
        try:
            winners = args_get_members(user, ctx.guild.members)
        except ValueError as e:
            await ctx.send(e.args[0])
            return
        uid = winners[0].id
        gid = ctx.guild.id
        resp = await self.stats(gid, uid)
        await ctx.send(resp)

    @commands.command(name="clear-stats", hidden=True)
    @commands.is_owner()
    async def clear_user_stats(self, ctx, *user):
        try:
            winners = args_get_members(user, ctx.guild.members)
        except ValueError as e:
            await ctx.send(e.args[0])
            return

        uid = winners[0].id
        gid = ctx.guild.id
        await self.clear_stats((gid, uid))

    @commands.command(name="clear-all-stats", hidden=True)
    @commands.is_owner()
    async def clear_all_stats(self, ctx):
        await self.clear_stats()

    async def clear_stats(self, user=None):
        from nike_bot.redis import UserStatsRedis

        u = UserStatsRedis()
        await u.connect()

        # Go ahead and clear our cache too
        user_cache.clear()

        if user:
            await u.reset_stats(user[0], user[1])
        else:
            await u.reset_ALL_stats()

    async def stats(self, gid, uid):
        # Get user Information
        user_stats = await user_cache[(gid, uid)]
        gconfig = await guild_cache[gid]

        ### Some basic parsing/formatting for the raw data # noqa
        # Split this up so we can show hour/min/sec etc.
        dd, hh, mm, ss = get_dd_hh_mm_ss(timedelta(seconds=user_stats.total_win_time))
        win_time = f"{dd} days, {hh} hours, {mm} minutes, and {ss} seconds"

        dd, hh, mm, ss = get_dd_hh_mm_ss(
            timedelta(seconds=user_stats.longest_time_streak)
        )
        time_streak = f"{dd} days, {hh} hours, {mm} minutes, and {ss} seconds"

        if user_stats.win_dates:
            win_dates = [
                str(datetime.utcfromtimestamp(x).date()) for x in user_stats.win_dates
            ]
            last_win = dt.date.today() - dt.date.fromtimestamp(user_stats.win_dates[-1])
            last_win = f"{last_win.days} days"

        else:
            win_dates = ""
            last_win = (
                "42, The Answer to the Ultimate Question of Life, "
                + "The Universe, and Everything"
            )

        if user_stats.mvp_dates:
            mvp_dates = [
                str(datetime.utcfromtimestamp(x).date()) for x in user_stats.mvp_dates
            ]
        else:
            mvp_dates = ""

        top_finishes = user_stats.wins + user_stats.second + user_stats.third
        if user_stats.entries:
            top_perc = f"{(top_finishes/user_stats.entries)*100:.2f}%"
        else:
            top_perc = ""

        role_str = f"Time in {gconfig.winner_role} Role"
        resp = user_stat_tmpl.render(
            role=role_str,
            entries=user_stats.entries,
            last_win=last_win,
            mvps=user_stats.mvps,
            mvp_dates=mvp_dates,
            wins=user_stats.wins,
            win_dates=win_dates,
            top_finishes=top_finishes,
            top_perc=top_perc,
            win_time=win_time,
            time_streak=time_streak,
        )

        return resp

    def get_stats_embed(self, stats):
        pass


async def setup(bot):
    await bot.add_cog(UserStats(bot))
