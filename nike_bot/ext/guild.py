import logging

from discord.ext import commands

from nike_bot.cache import guild_cache
from nike_bot.utils import has_manager_perms

from .templates import guild_config_tmpl


logger = logging.getLogger(__name__)


class GuildConfig(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="set-nike-config")
    @has_manager_perms(guild_cache)
    async def set_config(self, ctx, config_name, *value):
        guild_config = await guild_cache[ctx.guild.id]
        if len(value) == 1:
            value = value[0]
        logger.info("Received Config %s with args: %s", config_name, value)
        await guild_config.update(**{config_name: value})
        await ctx.send(f"Successfully set {config_name} to '{value}'")

    @commands.command(name="get-nike-config")
    @has_manager_perms(guild_cache)
    async def get_config(self, ctx):
        guild_config = await guild_cache[ctx.guild.id]

        rendered = guild_config_tmpl.render(configs=guild_config.as_dict())
        await ctx.send(rendered)

    async def cog_command_error(self, ctx, error):
        if isinstance(error, commands.CheckFailure):
            await ctx.send(
                "You don't have permissions for this command! Please contact an "
                "admin/owner if you think you should."
            )
        else:
            raise error


async def setup(bot):
    logger.info("Adding Guild command Cog")
    await bot.add_cog(GuildConfig(bot))
