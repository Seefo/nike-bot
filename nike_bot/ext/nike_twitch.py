import logging

from discord.utils import find
from twitchio.ext import commands as tcommands

from nike_bot.cache import tourney_cache
from nike_bot.config import env
from nike_bot.exceptions import NeedTournamentName, NoActiveTournaments

from .templates import twitch_help, twitch_tourney_info


logger = logging.getLogger(__name__)


class TwitchTournament(tcommands.Bot):
    # TwitchIO is...really bad.
    # Need to connect twitch and Discord channel

    def __init__(self, bot):
        self.dbot = bot
        super().__init__(
            irc_token=env.str("TWITCH_BOT_TOKEN"),
            client_id=env.str("TWITCH_CLIENT_ID"),
            nick="SeefoBot",
            prefix="!",
            initial_channels=["seefogaming"],
        )
        # when we add linking between discord and twitch, this will populate in redis
        # and we can look it up here (and cache it) if needed.
        # self.guild_map = TwitchDiscordCache()
        self.guild_map = {
            "seefogaming": 107313702054735872,
        }

    async def event_ready(self):
        logger.info("Twitch integration is ready!")

    @tcommands.command(name="player-join")
    async def add_t_player(self, ctx, player_name=None, tourney_name=None):
        # Users can optionally pass in their discord name to track results there.
        gid = self.guild_map[ctx.channel.name]

        try:
            if not tourney_name:
                tourney_name = await self.get_tournament_name(gid)
        except (NoActiveTournaments, NeedTournamentName) as e:
            await ctx.send(e.user_msg)
            return

        user = ctx.author.name if not player_name else player_name
        if player_name:
            if not self.in_discord(gid, user):
                await ctx.send(
                    f"{user} could not be found in Discord! Please join discord first. "
                    "If your discord name has spaces, please put your name in quotes."
                )
                return
        else:
            if not self.in_discord(gid, user):
                await ctx.send(
                    f"{user} could not be found in Discord, Cannot track results."
                )
            player_name = ctx.author.name

        logger.info("User '%s' Registering for Tournament '%s'", user, tourney_name)
        try:
            tm = self.dbot.get_cog("TournamentManager")
            await tm.update_player(gid, player_name, tourney_name, remove=False)
        except ValueError as e:
            await ctx.send(e.user_msg)
            return
        await ctx.send(f"Added {user} to the random pool for {tourney_name}")

    # @tcommands.command(name="team")
    # async def add_t_team(self, ctx, *, args):
    #    # 1st parsed arg is team name?
    #    # how do we handle tournament name in that case?
    #    args = parse_args(" ".join(args))
    #    team = args[0]
    #    players = args[1:]
    #    gid = self.guild_map[ctx.channel.name]

    #    for player in players:
    #        if not self.in_discord(gid,player):
    #            await ctx.send(
    #                f"{player} is not in {ctx.channel.name}'s Discord! "
    #                "If you want your stats to be tracked, please join the "
    #                "discord or use your discord name instead!"
    #            )

    @tcommands.command(name="tourney-help")
    async def tournament_help(self, ctx):
        # user requested, print out tournament info, else let them know none are
        # Active
        await ctx.send(twitch_help)

    @tcommands.command(name="tourneys")
    async def tournaments(self, ctx):
        """
        Periodically prints tournament join information for users in discord, when there
        is an active tournament.
        """
        gid = self.guild_map[ctx.channel.name]
        tourneys = await self.get_tournaments(gid)
        if tourneys:
            tourney_list = []
            for key in tourneys:
                logger.info("KEY: %s", key)
                tourney = await tourney_cache[key]
                tourney_list.append(
                    twitch_tourney_info.render(
                        name=tourney.name, player_count=len(tourney.players)
                    )
                )
            tourney_str = "Current active tournaments: " + ", ".join(tourney_list)
        else:
            tourney_str = "No active tournaments found!"
        await ctx.send(tourney_str)

    def in_discord(self, gid, player):
        guild = self.dbot.get_guild(gid)

        # TODO: replace this with a lookup
        # user = guild.get_member_named(player)
        user = find(lambda x: x.name.lower() == player.lower(), guild.members)
        return user

    async def get_tournament_name(self, gid):
        mngr = self.dbot.get_cog("TournamentManager")
        tourney = await mngr.get_tournament_name(gid)
        return tourney

    async def get_tournaments(self, gid):
        mngr = self.dbot.get_cog("TournamentManager")
        tourneys = await mngr.get_tournaments(gid)
        return tourneys


async def setup(bot):
    pass
