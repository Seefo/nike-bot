import asyncio
import logging

from functools import partial, wraps

from discord.ext import commands
from discord.utils import find

from nike_bot.exceptions import NotPlayerRole


logger = logging.getLogger(__name__)


def parse_args(comma_args):
    # comma separated
    args = [x.strip() for x in comma_args.split(",")]
    return args


def args_get_members(win_list, members):
    if isinstance(win_list, str):
        win_list = parse_args(win_list)
    logger.debug("Got member names: %s", win_list)
    winners = []
    # Filter through and find their User object
    for winner_name in win_list:
        if not winner_name:
            # avoid issues with many spaces or trailing commas or other shenanigans
            continue
        winner = find(lambda x: x.name.lower() == winner_name.lower(), members)
        if winner:
            winners.append(winner)
        else:
            raise ValueError(
                f"Could not find ${winner_name} in members list, please check for "
                "typos. If the name has spaces, please put it in quotes."
            )
    return winners


def get_dd_hh_mm_ss(timedelta):
    mm, ss = divmod(timedelta.seconds, 60)
    hh, mm = divmod(mm, 60)

    return timedelta.days, hh, mm, ss


async def check_manager(ctx, gconfig):
    is_owner = await ctx.bot.is_owner(ctx.author)
    if is_owner:
        return True

    if ctx.guild is None:
        return False

    user = ctx.author
    if gconfig.allow_mods:
        if user.guild_permissions.manage_guild is True:
            return True

    if gconfig.manager_roles:
        roles = [x.lower() for x in gconfig.manager_roles]
        print("ROLES: %s", roles)
        for role in roles:
            for urole in user.roles:
                if urole.name.lower() == role:
                    return True

    return False


def player_in_command_channel(gcache):
    async def pred(ctx):
        gconfig = await gcache[ctx.guild.id]
        for chan in gconfig.tourney_channel:
            if gconfig.tourney_channel:
                return ctx.channel.name.lower() == chan.lower()
            return True

    return commands.check(pred)


def has_player_role(gcache):
    async def pred(ctx):
        user = ctx.author
        gconfig = await gcache[ctx.guild.id]

        if not gconfig.player_role:
            # If they do not set a role, just assume anyone can join a tournament
            return True
        role = gconfig.player_role
        # for role in gconfig.player_role:
        if bool(find(lambda x: x.name.lower() == role.lower(), user.roles)):
            return True
        raise NotPlayerRole(
            f"Only members with {gconfig.player_role} role can join tournaments."
        )

    return commands.check(pred)


def has_manager_perms(gcache):
    async def pred(ctx):
        """
        If the user has the required role or the 'manage_guild' permission
        depending on this guild's configuration
        """
        gconfig = await gcache[ctx.guild.id]
        return await check_manager(ctx, gconfig)

    return commands.check(pred)


def async_wrap(func):
    @wraps(func)
    async def run(*args, loop=None, executor=None, **kwargs):
        if loop is None:
            loop = asyncio.get_event_loop()
        pfunc = partial(func, *args, **kwargs)
        return await loop.run_in_executor(executor, pfunc)

    return run
