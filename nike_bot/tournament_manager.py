import datetime as dt
import json
import logging
import random
import re

from enum import Enum, unique
from pprint import pformat

from pydash.arrays import chunk

from nike_bot.chal import ChallongeTournament as Chal
from nike_bot.exceptions import (
    InvalidTeam,
    InvalidTournamentState,
    PlayerOperationError,
)
from nike_bot.redis import TournamentRedis


# from .tournament_stats import TournamentStats


logger = logging.getLogger(__name__)


# TODO: Should this be a flag Enum?
@unique
class TournamentState(Enum):
    """
    Tournament States. These are numerical representations of the states
    for a tournament. The numeric values are stored in redis and should
    never change, but the names are OK to modify.
    """

    DOES_NOT_EXIST = 0
    OPEN = 10
    STARTED = 30
    FINISHED = 50
    CANCELLED = 70


@unique
class TournamentType(Enum):
    """
    Tournament Types. These are numerical representations of the tournaments
    we can support. The numeric values are stored in redis and should
    never change, but the names are OK to modify.
    """

    UNKNOWN = 0
    RL_3v3 = 20
    RL_2v2 = 21
    RL_1v1 = 22


TYPE_METADATA = {
    TournamentType.RL_3v3: {"team_size": 3},
    TournamentType.RL_2v2: {"team_size": 2},
    TournamentType.RL_1v1: {"team_size": 1},
}

# class Team:
#    def __init__(self,name,players=[],autogen=False):
#        self.id_ =
#        self.name = name
#        self.players = players
#        self.autogen = False


class TournamentManager:
    def __init__(self, key=("", ""), t_redis=None):
        self.gid, self.name = key
        self.t_redis = t_redis
        self.chal = Chal(self.name)
        self.tournament_stats = None

        self._version = 1
        self._team_counter = 1
        self._state = TournamentState.DOES_NOT_EXIST
        self._type = TournamentType.UNKNOWN
        self._unassigned = set()
        self._assigned = set()
        self._teams = dict()
        self._bracket_id = ""
        self._start_time = 0
        self._finish_time = 0

    @property
    def exists(self):
        return self._state is not TournamentState.DOES_NOT_EXIST

    @property
    def is_open(self):
        return self._state is TournamentState.OPEN

    @property
    def is_started(self):
        return self._state is TournamentState.STARTED

    @property
    def is_locked(self):
        return self._state is TournamentState.STARTED_LOCKED

    @property
    def is_finished(self):
        return self._state is TournamentState.FINISHED

    @property
    def is_cancelled(self):
        return self._state is TournamentState.CANCELLED

    @property
    def is_active(self):
        return self.is_open or self.is_started

    @property
    def players(self):
        return self._unassigned + self._assigned

    @property
    def teams(self):
        return self._teams.copy()

    @property
    def state(self):
        return self._state.name

    @property
    def type(self):
        return self._type.name

    @property
    def bracket(self):
        return self.chal.url

    @property
    def start_time(self):
        return self._start_time

    @property
    def finish_time(self):
        return self._finish_time

    @property
    def bracket_id(self):
        return self._bracket_id

    @classmethod
    async def from_json(cls, gid, json, t_redis=None, autoload=True):
        if not t_redis:
            t_redis = TournamentRedis()
            await t_redis.connect()
        else:
            t_redis = t_redis

        new = cls((gid, ""), t_redis)
        await new._json_load(json)
        return new

    @classmethod
    async def from_redis(cls, key, t_redis=None, autoload=True):
        if not t_redis:
            t_redis = TournamentRedis()
            await t_redis.connect()
        else:
            t_redis = t_redis

        new = cls(key, t_redis)
        if autoload:
            await new._redis_load()
        return new

    @classmethod
    async def from_challonge(cls, bracket_id, gid, t_redis=None, autoload=True):
        if not t_redis:
            t_redis = TournamentRedis()
            await t_redis.connect()
        else:
            t_redis = t_redis

        new = cls(t_redis=t_redis)
        new.gid = gid
        new._bracket_id = bracket_id
        await new.chal.load(bracket_id)
        await new._chal_load()
        return new

    async def _chal_load(self):
        await self.chal.load(self.bracket_id)
        self._state = TournamentState.OPEN
        # TODO: Figure out magic knowledge of 3s tournament
        self._type = TournamentType.RL_3v3

        self._start_time = self.chal.start_time
        self._finish_time = self.chal.finish_time
        self.name = self.chal.name
        # for team in
        # 1. Load start time/finish time
        # 2. Load all the players
        logger.info("Teams: %s", pformat(self.chal.participants))
        for team in self.chal.participants:
            team_name, players = team["display_name"].strip().rsplit(":", 1)
            # team_split = team["display_name"].split(":")
            team_name = team_name.lower()

            team_members = [m.strip() for m in re.split(r"/|\+", players)]
            await self.add_team(team_name, team_members)
        logger.info("Loaded Tournament from Challonge: %s", self.info())
        await self.store()
        # 3. ?????

    async def _redis_load(self):
        logger.info("Reloading tournament from Redis")
        raw_state = await self.t_redis.read_tournament(self.gid, self.name) or "{}"
        return await self._json_load(raw_state)

    async def _json_load(self, raw_json):
        state = json.loads(raw_json)
        logger.debug("Tournament state: %s", pformat(state))

        if state:
            # just make sure its not a request for a non-existant tournament
            state = self.update_version(state)

        self._version = state.get("version",0)
        self._state = TournamentState(state.get("state", 0))
        self._type = TournamentType(state.get("type", 0))
        self._unassigned = state.get("unassigned", [])
        self._assigned = state.get("assigned", [])
        self._teams = state.get("teams",{})
        self._team_counter = state.get("auto_counter",1)
        self._start_time = dt.datetime.utcfromtimestamp(state.get("start_time", 0.0))
        self._finish_time = dt.datetime.utcfromtimestamp(state.get("finish_time", 0))

        if self.is_cancelled:
            state["bracket_id"] = ""
        if state.get("bracket_id", None):
            self._bracket_id = state["bracket_id"]
            await self.chal.load(self._bracket_id)

        # Get this guild's stats so we can update it when we are done
        # self.tournament_stats = TournamentStats(self.gid)

    def update_version(self,state):
        sver = state.get("version",0)
        if sver != self._version:
            logger.info("Converting Tournament version from %s to %s",sver,self._version)
            converter = getattr(self,f"convert_v{sver}_to_v{self._version}",None)
            if not converter:
                raise UnsupportedVersionUpdate(
                    f"Tournament version {sver} cannot be updated to {self._version}")
            state = converter(state)
        return state
    def convert_v0_to_v1(self,state):
        teams = state.get("auto_teams",{})
        teams.update(state.get("formed_teams",{}))
        state["teams"] = teams
        state["version"]  = 1
        return state
        # don't need to do the auto counter...

    async def store(self):
        d = self.to_json()
        await self.t_redis.update_tournament(self.gid, self.name, d)

    def to_json(self):
        d = dict(
            version=self._version,
            state=self._state.value,
            type=self._type.value,
            unassigned=list(self._unassigned),
            assigned=list(self._assigned),
            teams=self._teams,
            auto_counter=self._team_counter,
            bracket_id=self._bracket_id,
            start_time=self._start_time.timestamp(),
            finish_time=self._finish_time.timestamp(),
        )
        jsonified = json.dumps(d)
        return jsonified

    ####################
    # Tournament State #
    ####################
    async def create(self, type_=TournamentType.RL_3v3, create_bracket=True):
        if self.is_active or self.is_finished:
            raise InvalidTournamentState(
                "A recent tournament shares this name, please pick a new name!"
            )

        # Use our challonge API token to create a bracket
        if create_bracket:
            self._bracket_id = await self.chal.generate(self.name)
            logger.debug("Created bracket @ %s", self.bracket)

        self._state = TournamentState.OPEN
        self._type = type_
        await self.t_redis.create_tournament(self.gid, self.name, self.to_json())

    async def start(self, generate_teams=False):
        if self.is_started:
            raise InvalidTournamentState("Tournament started already!")
        if generate_teams:
            await self.update_teams()
        # If challonge integration is working, lets bulk add the teams
        if self.bracket:
            teams = []
            for tn, pl in self.teams.items():
                pl = "/".join(pl)
                teams.append(f"{tn}: {pl}")
            logger.info("Bulk adding: %s", teams)
            await self.chal.bulk_join(teams)
            # go ahead and start, if there is a case where we need to randomize bracket
            # admins can manually do that, but in like 95% of cases, we dont need to.
            await self.chal.start()
        self._state = TournamentState.STARTED
        self._start_time = dt.datetime.now()
        await self.store()

    async def unlock(self):
        if not self.is_started:
            # nothing to do here.
            return
        await self.chal.reset()
        self._state = TournamentState.OPEN

    async def finish(self, date=None):
        # Close challonge bracket?
        # we could handle winner interface here too...
        # but I think its better to keep that separate
        if self.is_finished:
            raise InvalidTournamentState("Tournament has already finished!")
        if not self.is_active:
            raise InvalidTournamentState(f"Tournament is {self._state}, cannot finish!")

        if self._bracket_id:
            results = await self.chal.finish()
            # Results may have old player data, so lets put OUR player info in place
            modified_results = {}
            for position,res in results.items():
                modified_results[position] = { 
                    "team" : res["team"],
                    "players" : self._teams[res["team"]],
                }
            results = modified_results
        else:
            results = {}
        if not self._finish_time.timestamp():
            if not date:
                self._finish_time = dt.datetime.now()
            else:
                self._finish_time = date
        self._state = TournamentState.FINISHED
        await self.t_redis.archive_tourney(self.gid, self.name, self.to_json())
        # await self.tournament_stats.update_stats(
        # self.start_time,self.finish_time,self.players)
        # TODO: Return 1st/2nd/3rd place
        return results

    async def cancel(self):
        self._state = TournamentState.CANCELLED
        await self.t_redis.delete_tournament(self.gid, self.name)
        if self._bracket_id:
            await self.chal.destroy()

    #####################################
    # Team Randomization/creation logic #
    #####################################
    def _create_teams_from_unassigned(self):
        team_size = TYPE_METADATA[self._type]["team_size"]
        unassigned = self._unassigned
        if not unassigned:
            return {}, []
        logger.info(
            "Found %s unassigned players. Create %s sized teams",
            len(unassigned),
            team_size,
        )

        # randomize the teams and create chunks
        random.shuffle(unassigned)
        team_list = chunk(unassigned, team_size)

        # Handle any leftovers
        if len(team_list[-1]) != team_size:
            incomplete = team_list.pop(-1)
        else:
            incomplete = []

        # Assign them some names
        teams = {}
        for team in team_list:
            teams[f"Team {self._team_counter:02}"] = team
            self._team_counter += 1

        return teams, incomplete

    async def update_teams(self):
        if not self.exists or self.is_cancelled or self.is_finished:
            raise InvalidTournamentState("Tournament finished or cancelled, can't create teams!")

        teams, incomplete = self._create_teams_from_unassigned()
        logger.debug("Created %s teams, with %s leftover", teams, incomplete)

        for name, team in teams.items():
            await self.add_team(name, team)

        # Update our book keeping
        await self.store()

        # Return ALL teams and any incomplete teams
        return self.teams, incomplete

    ########################
    # manage players/teams #
    ########################
    async def add_player(self, player):
        if not self.is_open:
            raise InvalidTournamentState(
                f"Tournament {self.name} is {self.state}. A mod needs to add the" + \
                " player directly to their team using the substitute command!"
            )

        if player in self._assigned:
            raise PlayerOperationError("Player already on a team. Not adding to random pool")

        if player in self._unassigned:
            #Player already in the list, just skip
            return
        else:
            # Otherwise add them and store
            self._unassigned.append(player)

        await self.store()

    async def remove_player(self, player):
        if player in self._assigned:

            team = self.find_players_team(player)
            await self.remove_from_team(team,player)
        else:
            self._unassigned.remove(player)
            await self.store()


    ##########################
    # Team specific commands #
    ##########################

    async def swap_players(self,player1,player2):
        """
        current Tournament structure doesn't help this command much, so we brute force
        a lot of it. In the future, this command will get a lot more efficient
        """
        team1 = self.find_players_team(player1)
        team2 = self.find_players_team(player2)

        logger.debug("p1: %s, p2: %s...found teams (1: %s, 2: %s)",player1,player2,team1,team2)

        if team1 and team2:
            self._remove_from_team(team1,player1)
            self._remove_from_team(team2,player2)
            self._add_to_team(team1,player2)
            self._add_to_team(team2,player1)
        elif team1:
            self._remove_from_team(team1,player1)
            self._add_to_team(team1,player2)
        elif team2:
            self._remove_from_team(team2,player2)
            self._add_to_team(team2,player1)
        else:
           raise PlayerOperationError("Didn't find either player on any teams...")

        await self.store()


    def _add_to_team(self,team_name,player):
        self._teams[team_name].append(player)
        self._assigned.append(player)
        if player in self._unassigned:
            self._unassigned.remove(player)
        # Remove any duplicates
        self._assigned = list(set(self._assigned))

    def _remove_from_team(self,team_name, player):
        self._teams[team_name].remove(player)
        self._assigned.remove(player)

    async def add_to_team(self,team_name,players):
        """
        """

        if isinstance(players,str):
            players = [players]
        else:
            players = list(players)

        # Check if any of the players are already on a team
        already_assigned = []
        for player in players:
            if player in self._assigned:
                already_assigned.append(player)
                continue
        adds = [x for x in players if x not in already_assigned] 
        self._add_to_team(team_name, adds)

        await self.store()
        # Let discord know somethings couldn't be done
        if already_assigned:
            raise IncompleteTeamChange(
                f"'{already_assigned}' where skipped as they are already on a team"
            )

    async def remove_from_team(self,team_name,players):
        if isinstance(players,str):
            players = [players]
        else:
            players = list(players)

        removed = []
        not_found = []
        for player in players:
            if player in self._teams[team_name]:
                self._remove_from_team(team_name,player)

        await self.store()

    async def add_team(self, team_name, players):

        # check if any players are already on a team, reject if so.
        already_assigned = set(players).intersection(self._assigned)
        if already_assigned:
            raise InvalidTeam(
                f"Some players '{already_assigned}' are already on a team. Use substitute to move them."
            )

        self._teams[team_name] = []
        self._team_counter += 1
        # Put players in the assigned list
        for player in players:
            self._add_to_team(team_name,player)
            # If the player was previously in the random pool, remove.
        await self.store()

    async def remove_team(self, team_name):
        if not self.is_open:
            raise InvalidTournamentState(
                f"Tournament {self.name} is {self.state}. Removing a team isn't"+\
                " supported, put them as a loss in the bracket and continue"
            )
        try:
            for player in self._teams[team_name]:
                self._assigned.remove(player)

            self._teams.pop(team_name)
        except KeyError:
            raise ValueError(f"Team {team_name} is not currently in this tournament")

        await self.store()

    async def change_team_name(self, team, new_name):
        if team in self._teams:
            self._teams[new_name] = self._teams.pop(team)
        else:
            raise KeyError(f"{team} could not be found in list of teams")

        await self.store()

    ######################
    # Assorted utilities #
    ######################
    def find_players_team(self, player):
        # we always store player names in lowercase
        for k, v in self.teams.items():
            if player in v:
                return k
        return None

    def get_team(self, name):
        return self.teams[name].copy()

    def info(self):
        d = dict(
            player_count=len(self.players),
            current_state=self.state,
            type=self.type,
            teams=self.teams,
            players=self.players,
            bracket=self.bracket,
        )
        return d
