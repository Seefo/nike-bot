import logging

from nike_bot.config import env

from .redis import Redis


logger = logging.getLogger(__name__)

ZSET_NAME = "TIMERS"


class WinTimerRedis(Redis):
    # CRUD
    def __init__(self, url=None, db=None):
        if not url:
            url = env.str("REDIS_U_URL")

        if not db:
            db = env.int("REDIS_U_DB")

        super().__init__(url, db)

    @staticmethod
    def ukey(gid, uid):
        return f"USER_{uid}_{gid}"

    async def add_winner_timeout(self, gid, uid, score):
        name = self.ukey(gid, uid)
        winners = {name: score}
        await self.redis.zadd(ZSET_NAME, winners)

    async def add_winners_timeout(self, gid, winners):
        win_map = {}
        for name, score in winners:
            win_map[self.ukey(gid, name)] = score
        await self.redis.zadd(ZSET_NAME, win_map)

    async def check_timeouts(self, score):
        f = await self.redis.zrangebyscore(
            ZSET_NAME, min=0, max=score, withscores=True, start=0, num=50
        )
        users = []
        for key, score in f:
            key = key.split("_")
            # tuple of ints ( gid, uid)
            users.append(((int(key[2]), int(key[1])), score))

        return users

    async def delete_timeouts(self, score):
        await self.redis.zremrangebyscore(ZSET_NAME, 0, score)
