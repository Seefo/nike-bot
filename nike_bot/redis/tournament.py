import asyncio
import logging

from nike_bot.config import env

from .redis import Redis


logger = logging.getLogger(__name__)

TOURNAMENT_TIMEOUT = 7 * 24 * 60 * 60  # 7 days


class TournamentRedis(Redis):
    # CRUD
    def __init__(self, url=None, db=None):
        if not url:
            url = env.str("REDIS_G_URL")

        if not db:
            db = env.int("REDIS_G_DB")

        super().__init__(url, db)

    @staticmethod
    def akey(gid):
        return f"ACTIVE_TOURNAMENTS_{gid}"

    @staticmethod
    def gkey(gid):
        return f"TOURNAMENTS_{gid}"

    async def read_tournament(self, gid, name):
        key = self.akey(gid)
        tstate = await self.redis.hget(key, name)
        return tstate

    async def create_tournament(self, gid, name, tourney):
        tasks = []
        tasks.append(self.update_tournament(gid, name, tourney))
        await asyncio.gather(*tasks)

    async def update_tournament(self, gid, name, tourney):
        tasks = []
        key = self.akey(gid)
        tasks.append(self.redis.hset(key, name, tourney))
        await asyncio.gather(*tasks)

    async def archive_tourney(self, gid, name, tourney):
        """
        Move from active tournaments to the long term storage.
        """
        tasks = []
        logger.info("GID: %s", gid)
        logger.info("GID: %s", self.gkey(gid))
        tasks.append(self.redis.hset(self.gkey(gid), name, tourney))
        tasks.append(self.redis.hdel(self.akey(gid), name))
        await asyncio.gather(*tasks)

    async def delete_tournament(self, gid, name):
        return await self.redis.hdel(self.akey(gid), name)

    async def read_active_tournaments(self, gid):
        tournaments = await self.redis.hkeys(self.akey(gid))
        return tournaments

    async def read_tournaments(self, gid, values=False):
        if values:
            tournaments = await self.redis.hgetall(self.gkey(gid))
        else:
            tournaments = await self.redis.hkeys(self.gkey(gid))
        return tournaments

    # Stats related
    @staticmethod
    def ukey(gid, uid):
        return f"GUILD_STATS_{gid}"

    async def update_stats(self, gid, uid, **stats):
        key = self.ukey(gid, uid)
        return await self.redis.hmset_dict(key, **stats)

    async def get_stats(self, gid, uid):
        key = self.ukey(gid, uid)
        stats = await self.redis.hgetall(key)
        return stats

    async def reset_stats(self, gid, uid):
        key = self.ukey(gid, uid)
        await self.redis.unlink(key)

    async def reset_ALL_stats(self):
        cur = b"0"  # set initial cursor to 0
        while cur:
            cur, keys = await self.redis.scan(cur, match=self.ukey("*"))
            if keys:
                keys = tuple(keys)
                await self.redis.unlink(*keys)
