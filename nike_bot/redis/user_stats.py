import logging

from nike_bot.config import env

from .redis import Redis


logger = logging.getLogger(__name__)


class UserStatsRedis(Redis):
    # CRUD
    def __init__(self, url=None, db=None):
        if not url:
            url = env.str("REDIS_U_URL")

        if not db:
            db = env.int("REDIS_U_DB")

        super().__init__(url, db)

    @staticmethod
    def ukey(gid, uid):
        return f"USER_{gid}_{uid}"

    async def update_stats(self, gid, uid, **stats):
        key = self.ukey(gid, uid)
        return await self.redis.hset(key, mapping=stats)

    async def get_stats(self, gid, uid):
        key = self.ukey(gid, uid)
        stats = await self.redis.hgetall(key)
        return stats

    async def reset_stats(self, gid, uid):
        key = self.ukey(gid, uid)
        await self.redis.unlink(key)

    async def reset_ALL_stats(self):
        cur = b"0"  # set initial cursor to 0
        while cur:
            cur, keys = await self.redis.scan(cur, match=self.ukey("*", "*"))
            if keys:
                keys = tuple(keys)
                await self.redis.unlink(*keys)
