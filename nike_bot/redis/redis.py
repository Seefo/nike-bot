from redis import asyncio as aioredis


class Redis:
    def __init__(self, url, db):
        self.url = url
        self.db = db
        self.redis = None

    async def connect(self):
        # This used to be a decorator, but i dropped it cause it
        # made testing ass

        if not self.redis:
            self.redis = await aioredis.from_url(
                self.url, db=self.db, encoding="utf-8", decode_responses=True
            )
        return self.redis

    def close(self):
        # Just for API purposes, apparently we dont need to close conn. anymore
        pass

    async def wait_closed(self):
        # Just for API purposes, apparently we dont need to close conn. anymore
        pass
        # self.redis.close()
        # return await self.redis.wait_closed()
