import logging

from nike_bot.config import env

from .redis import Redis


# CRUD
logger = logging.getLogger(__name__)


class GuildConfigRedis(Redis):
    def __init__(self, url=None, db=None):
        if not url:
            url = env.str("REDIS_G_URL")

        if not db:
            db = env.int("REDIS_G_DB")
        super().__init__(url, db)

    @staticmethod
    def gkey(gid):
        return f"GUILD_{gid}"

    async def read_guild_config(self, gid):
        """
        reads a guild config from redis.
        """
        key = self.gkey(gid)
        config_dict = await self.redis.hgetall(key)
        return config_dict

    async def create_guild_config(self, gid, **kwds):
        return await self.update_guild_config(gid, **kwds)

    async def update_guild_config(self, gid, **config):
        key = self.gkey(gid)
        logger.info("Redis update for key %s with %s", key, config)
        return await self.redis.hset(key, mapping=config)

    async def delete_guild_config(self, gid):
        pass

    async def read_guild_key(self, gid, key):
        pass

    async def update_guild_key(self, gid, key, value):
        pass
